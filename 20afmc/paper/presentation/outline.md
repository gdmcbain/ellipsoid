Ideas
=====

  * __Introduction__
	* Benchmarks on natural convection in horizontally heated cavities
	* Some cavities are round
	* Solutions for spherical cavities (McBain & Stephens 2000; McBain 2001)
	* Here: generalize to ellipse
  * __Mathematical formulation__
  * __Solution__
	* _Thermal conduction in the surrounding solid_
	* _Conduction and creeping flow_
	* _Two-dimensional convection in an ellipse_
	  * Two-dimensional solution in ellipse, by analogy with Bryan's plate.
	  * Unidirectional forced flow in ellipse (Mathieu 1853):
        expressed as Cartesian polynomial, with Theta as factor
	* _Creeping pressure in the ellipse and sphere_
	  * Pressure in ellipse like xy, just like sphere! (figure 3)
    * _Creeping flow in the ellipsoid_
	  * Creeping velocity from xy pressure
	  * Fully-developed flow in side-heated vertical elliptic duct
        (McBain 1999)
    * _First-order correction to the temperature_
	  * solution (figure 4)
    * _The temperature to first order_
      * solution (figure 5)
  * __Discussion & conclusions__
  
	  
Corresponding images
====================

  1. Side-heated square/rectangular cavity?  Whence?  From our own
     publication?
  2. Spherical cavity (McBain & Stephens 2000; McBain 2001)
  3. Defining sketch (figure 1)

  4. Temperature in the surrounding solid (Firedrake?).  Maybe use
     halfspace outside ellipsoid; viewed along the -z-axis, that
     should show the isotherms normal to x nicely.
  5. Stream-lines in ellipse (figure 2)
  6. Forced flow in ellipse (Mathieu 1853)
  7. Pressure in side-heated ellipse (figure 3)
  8. Bouyant flow in vertical side-heated ellipse (McBain 1999)
  9. Creeping solution for ellipsoid (?)
  10. First-order correction to temperature (figure 4)
  11. Temperature to first order (figure 5)
  

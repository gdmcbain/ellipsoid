#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Stream-lines in the ellipse, after Bryan.

For 20AFMC.

:author: G. D. McBain <gdmcbain@protonmail.com>

:created: 2016-08-04

Now, psi = psi0 F**2, so stream-lines are psi0 F**2 = c, for constant
c, or F**2 = c / psi0, so (x/a)**2 + (y/b)**2 = 1 + c / psi0, [x/{a *
sqrt(1 + c/psi0)}]**2 + [y/{b + sqrt(1 + c/psi0)}]**2 = 1.  For c = 0,
this reduces to F = 0, the boundary so a(c) = a(0) * sqrt(1 + c/psi0),
and similarly for b(c).  Now we need F < 0 to be inside, and F = -1 is
the centre, so the centre is c = psi0

'''

from __future__ import absolute_import, division, print_function

from os.path import splitext
from sys import argv

from matplotlib.collections import PatchCollection, EllipseCollection
from matplotlib.patches import Ellipse
import matplotlib.pyplot as plt

import numpy as np
from scipy.constants import golden


def main(name, ar=golden - 1):

    x = np.linspace(-1, 1)
    y = ar * x
    X, Y = np.meshgrid(x, y)

    fig, ax0 = plt.subplots()
    patches = [Ellipse((0, 0),
                       2 * np.sqrt(1 - c), 2 * ar * np.sqrt(1 - c),
                       fill=False,
                       linewidth=4 if c == 0 else 1)
               for c in np.linspace(0, 1, 12)]
    ax0.add_collection(PatchCollection(patches, True))
    ax0.set_aspect(1)
    ax0.set_xlim(np.array([-1., 1.]) * 1.02)
    ax0.set_ylim(ar * np.array(ax0.get_xlim()))
    plt.axis('off')
    print(ax0.get_xlim())
    fig.savefig(name, bbox_inches='tight')


if __name__ == '__main__':
    main(splitext(argv[0])[0], 1/2)

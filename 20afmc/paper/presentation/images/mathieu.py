#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Contours of velocity in the duct of elliptic section.

For 20AFMC.

:author: G. D. McBain <gdmcbain@protonmail.com>

:created: 2016-12-02


'''

from __future__ import absolute_import, division, print_function

from os.path import splitext
from sys import argv

from matplotlib.patches import Ellipse
import matplotlib.pyplot as plt

import numpy as np
from scipy.constants import golden

def main(name, ar=golden - 1):

    a, b, c = 1, ar, ar**2
    x = np.linspace(-a, a)
    X, Y = np.meshgrid(x, ar * x)

    w = a**2 * b**2 - a**2 * Y**2 - b**2 * X**2

    fig, ax0 = plt.subplots()
    boundary = Ellipse((0, 0), 2 * a, 2 * b, fill=False, linewidth=4)
    ax0.add_patch(boundary)

    cs = ax0.contour(X, Y, w, 2**5, cmap=plt.get_cmap('bwr'))

    for collection in cs.collections:
        collection.set_clip_path(boundary)

    ax0.set_aspect(1)
    ax0.set_xlim(np.array([-1., 1.]) * 1.02)
    ax0.set_ylim(ar * np.array(ax0.get_xlim()))
    plt.axis('off')
    fig.savefig(name + '.png', bbox_inches='tight')

    
if __name__ == '__main__':
    main(splitext(argv[0])[0], 1/2)

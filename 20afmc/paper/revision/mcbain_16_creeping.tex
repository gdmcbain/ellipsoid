%
%    20afmc template paper adapted from 19afmc template.
%
%    Format: LaTeX2e.
%
\documentclass[twocolumn]{afmc_art}
\usepackage{graphicx, amsmath, bm, url}
\bibliographystyle{afmc}
%
%
\begin{document}

%
%%%  Fill in following information.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%    Contact Author: put your name here
%            E-mail: put your email address here
%             Phone: put your phone number here
%               Fax: put your fax number here
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%  Put your definitions (if any) here
%
%
%%%  Title goes here - do not force a line-break unless ABSOLUTELY necessary
%
\title{Creeping Convection in a Horizontally Heated Ellipsoid}
%
%%%  Authors and affiliations with brief address
%
%    format:
%
\author{G.~D. McBain}

\affiliation{Modelling \& Simulation, Memjet Australia Pty Ltd,
  Macquarie Park, New South Wales 2113, Australia}

%
\maketitle
%
%%%  Paper proper starts here
%
\section{Abstract}
Natural convection in horizontally heated ellipsoidal cavities is
considered in the low Grashof number limit, solving the Laplace
equation for steady thermal conduction in the unbounded solid
exterior, the Oberbeck--Boussinesq equations in the fluid-filled
interior, and matching the temperature at the interface.

In the hierarchy of equations governing the asymptotic expansion for
small Grashof number, at each order a forced Stokes problem must be
solved for the momentum correction. The creeping flow is known for the
sphere in closed form in terms of the toroidal and poloidal
potentials, spherical coordinates, and spherical harmonics.  Rather
than attempting to generalize this to ellipsoidal coordinates, it is
re-expressed in terms of the primitive pressure-velocity variables as
polynomials in the Cartesian coordinates.  This form, equivalent in
the sphere, suggests solutions for the pressure in an ellipsoid, which
can then be found together with the velocity in closed form by the
method of undetermined coefficients.  Similarly, the perturbations to
the temperature satisfy Poisson equations which can be solved by the
same method. Polynomial formul\ae\ are given for the creeping flow and
the first-order correction to the temperature.

In the limit as one of the axes of the ellipsoid tends to infinity,
the three-dimensional solution reduces to a two-dimensional solution
for natural convection in a horizontal elliptical cylinder,
tranversely horizontally heated.  This exact solution is believed to
be new too.

\section{Introduction}
Natural convection in horizontally heated cavities is a much studied
subject~\cite[ch.~5]{bejan_95_convection}, often used as a test-case
for computational fluid dynamics~\cite{devahldavis_83_natural,
  davies_11_fluidity}.

Most often, rectangular cavities are studied, either because of
relevance to applications like double-glazed
windows~\cite{batchelor_54_heat, jiracheewanun_06_natural}, or simply
because that facilitates meshing, particularly structured meshing.
Some cavities are round though, as in natural subterranean
reservoirs~\cite{ostroumov_58_free} or in closed-porous insulation
materials~\cite{lewis_50_free}.  Revisiting those
problems~\cite{mcbain_01_convection, mcbain_00_low}
revealed that the sphere actually proved more amenable to analysis
than the box, admitting closed-form solutions for the creeping-flow
limit.

The present paper shows how the spherical solution may be generalized
to the ellipsoidal cavity.  Surprisingly, this generalization,
expressing the primitive velocity--pressure dependent variables as
polynomials of the Cartesian coordinates turns out to be simpler than
the previous closed-form solutions for the sphere, using spherical
harmonics, spherical coordinates, and toroidal and poloidal
potentials.

\section{Mathematical formulation}
Consider an ellipsoidal cavity defined by $\varTheta < 0$ with
\begin{equation}
\varTheta \equiv \frac{x^2}{a^2} + \frac{y^2}{b^2} + \frac{z^2}{c^2} -
1, \label{eq:varTheta}
\end{equation}
as shown in figure~\ref{fig:geometry}, oriented with one axis vertical
with respect to the uniform gravitational field $\mathbf g = -g\mathbf
j$ in a uniform infinite solid subject to an overall horizontal
temperature gradient $\bm\nabla T \sim T_{x,\infty} \mathbf i$
parallel to another of the cavity's axes.

In the surrounding solid, the steady temperature satisfies Laplace's
equation, $\nabla^2 T = 0$; in the cavity, the temperature is coupled
to the velocity $\bm u$ and pressure $p$ by the Oberbeck--Boussinesq
equations
\begin{align}
  \nabla\cdot \bm u &= 0 \\
  \rho\bm u\cdot\nabla\bm u &= -\bm\nabla p
  + \rho g\beta T\mathbf j + \mu\nabla^2\bm u \\
  \rho C \bm u\cdot\nabla T &= k\nabla^2 T,  \label{eq:T}
\end{align}
where $\rho$, $C$, $\beta$, $\mu$, and $k$ are the density,
specific heat, and coefficients of thermal volumetric expansion,
viscosity, and thermal conductivity of the fluid.  The temperature $T$
is expressed as the excess over that the centre of the cavity
(constant, by symmetry, and equal to the value that would prevail
without the cavity).

Guided by the work on the spherical special
case~\cite{mcbain_01_convection, mcbain_00_low}, solutions are sought
for the limits in which the solid is a much better conductor than the
cavity and in which the cavity is small.


\section{Solution}
\subsection{Thermal conduction in the surrounding solid}
Insofar as the solid is a much better thermal conductor than the
fluid, it sees the cavity as an insulator and so the outward
temperature gradient is normal to the ellipsoid.  The temperature,
therefore, is analogous to the velocity potential for ideal flow over
an ellipsoid with freestream parallel to the
$x$-axis~\cite{green_35_researches, lamb_32_hydrodynamics}.  This
solution has the temperature on the ellipsoid proportional to $x$, but
with a different gradient to that at infinity (higher, as the
heat-lines bulge around the adiabatic cavity).

%% The temperature in the solid is shown in figure~\ref{fig:geometry}.

%% The bit in Lamb citing Green's (1835, miscited as 1883) (?!?)
%% solution is pp. 152--153.

\begin{figure}
  \includegraphics[width=\columnwidth]{geometry.eps}
  \caption{A ellipsoid with arbitrary ratios of axes, defined by the
    zero level-set of equation~(\ref{eq:varTheta}), aligned with the
    Cartesian coordinate system.  Here a temperature gradient is
    applied in the $x$-direction and gravity in the
    $-y$-direction.}\label{fig:geometry}
\end{figure}

Insofar as the solid is a much better thermal conductor than the
fluid, this serves to define a Dirichlet temperature boundary
condition on the ellipsoid for the latter, say $T = x\Delta T/2a$.

\subsection{Conduction and creeping flow}
The limiting solution in the cavity as $\Delta T\rightarrow 0$ is
stagnant ($\bm u \sim \mathbf 0$) conduction ($T \sim x\Delta
T/2a$)~\cite[\S\,27]{ferrers_77_ellipsoidal}.

Beginning the perturbation process by
iteration~\cite[p.~2]{hinch_91_perturbation}
yields the Stokes problem:
\begin{align}
  \nabla\cdot \bm u_0 &= 0 \\
  -\bm\nabla p_0 + \mu\nabla^2\bm u_0 &=
  - \rho g\beta\Delta T x\mathbf j/2a.\label{eq:Stokes}
\end{align}

\subsection{Two-dimensional convection in an ellipse}
If $c\gg a, b$, the ellipsoid defined by equation~(\ref{eq:varTheta})
becomes two-dimensional.  Ignoring the $z$-dimension, a
stream-function $\psi (x, y)$ can be introduced such that $\bm u_0 =
\nabla\times (\psi\mathbf k)$.  Taking the $z$-component of the curl
of equation~(\ref{eq:Stokes}) leads to the biharmonic equation
$\mu\nabla^4\psi = -\rho g\beta \Delta T/2a$ with $\psi$ and its
gradient vanishing on the boundary.  As noted by
Batchelor~\cite{batchelor_54_heat} for the corresponding problem in a
rectangle, this is analogous to the boundary value problem of bending
a clamped plate, with $\psi$ there representing the deflexion.  That
problem was solved in the ellipse by Bryan~\cite{love_44_treatise,
  mansfield_64_bending}; thus
\begin{equation}
  \psi = -\frac{\rho g\beta \Delta T a^3b^4\varTheta^2}{16\mu\left(
    3a^4 + 2a^2 b^2 + 3b^4\right)}.
\label{eq:Bryan}
\end{equation}

To the best of our knowledge, this has not been previously reported as
the solution for creeping convection in the ellipse.

The level-sets of equation~(\ref{eq:Bryan}) are drawn in
figure~\ref{fig:Bryan}.

\begin{figure}
  \hspace{-1.5em}%
  \centering\includegraphics[width=\columnwidth]{bryan.eps}\vspace{-3ex}
  \caption{Stream-lines of the creeping flow in the ellipse, derived
    from the stream-function of
    equation~(\ref{eq:Bryan}).}\label{fig:Bryan}
\end{figure}

Note the role of the boundary function $\varTheta$ from
equation~(\ref{eq:varTheta}): its square appears for the two boundary
conditions on the stream-function, $\psi = 0$ and
$\partial\psi/\partial n = 0$.  Similarly, for the Poisson equation in
an ellipse for the fully developed velocity in a duct of elliptic
section, the solution is proportional to the first power of
$\varTheta$ in order to satisfy the single no-slip
condition~\cite{mathieu_63_mouvement, boussinesq_68_memoire,
  mcbain_99_fully}.

This solution of the two-dimensional problem by means of the
stream-function appears to be a dead end, not leading to the
ellipsoid.  Its generalization to a three-dimensional vector-potential
is tractable in the sphere using the toroidal and poloidal
potentials~\cite{mcbain_01_convection}, but adapting
those to the ellipsoid seems forbidding; the study of ellipsoidal
harmonic functions is `more complicated by far than the corresponding
study of' spherical harmonic
functions~\cite[p.~xi]{dassios_12_ellipsoidal}.

%% \textit{TODO:---Look up to see whether there actually is an
%%   ellipsoidal generalization of the toroidal and poloidal potentials.}

\subsection{Creeping pressure in the ellipse and sphere}
After computing the velocity from the stream-function of
equation~(\ref{eq:Bryan}), the corresponding pressure $p_0$ can be
computed by integrating equation~(\ref{eq:Stokes}):
\begin{equation}
  p_0 = \frac{\rho g\beta\Delta T a (3a^2 + b^2) xy}{
    2\left(3a^4 + 2a^2 b^2 + 3b^4\right)}. \label{soln:p0-2d}
\end{equation}

The pressure from equation~(\ref{soln:p0-2d}) is drawn in
figure~\ref{fig:p0-2d}.

\begin{figure}
  \hspace{-1em}\includegraphics[width=\columnwidth]{ellipse.eps}\vspace{-3ex}
  \caption{Isobars of the creeping flow in the ellipse from
    equation~(\ref{soln:p0-2d}); the sign is that of $xy$ in each
    quadrant.}
  \label{fig:p0-2d}
\end{figure}

The Stokes pressure $p_0$ was given in the special three-dimensional
case of the sphere~\cite{mcbain_01_convection} as also proportional to
$xy$.  That the simple symmetry of the function $xy$ describes the
creeping pressure in both the ellipse and the sphere motivates trying
it for the general ellipsoid in a method of undetermined coefficients.

In a sense, in the use here of Cartesian coordinates, we are taking
the opposite approach to Lam\'e, for whom in general `the study of a
physical problem lead to that of a system of curvilinear
coordinates'~\cite{guitart_09_coordonnees}.

\subsection{Creeping flow in the ellipsoid}
For a given pressure, the Stokes equation~(\ref{eq:Stokes}) reduces to
a vector Poisson equation for the velocity or a scalar Poisson
equation for each component; say $p_0 = P_0 xy$ for some
constant $P_0$, then
\begin{align}
  \mu\nabla^2 u_0 &= P_0 y \label{eq:u0} \\
  \mu\nabla^2 v_0 &=
    \left(P_0 - \frac{\rho g \beta \Delta T}{2a}\right) x\label{eq:v0} \\
  \mu\nabla^2 w_0 &= 0
  \end{align}
with $u_0 = v_0 = w_0 = 0$ on $\varTheta=0$. Beside the trivial $w_0 =
0$, the first two Poisson equations~(\ref{eq:u0})--(\ref{eq:v0})
recall the two-dimensional Poisson equation for the fully developed
natural convection in a tall horizontally heated vertical cavity of
elliptic section~\cite{mcbain_99_fully}, suggesting solutions
proportional to $y\varTheta$ and $x\varTheta$, respectively, with the
boundary function $\varTheta$ as a factor enforcing the no-slip
condition and the coefficients of proportionality depending affinely
on the undetermined $P_0$.  The latter is determined by requiring that
the velocity be divergence-free.  The result is:
\begin{align}
  \bm u_0 &= \frac{\rho g \beta \Delta T ab^2c^2\left(
  a^2y\mathbf i - b^2x\mathbf j
  \right) \varTheta}{
    4\mu\left(a^4 b^2 + 3a^4c^2 + a^2b^4 + 2a^2b^2c^2 + 3b^4c^2\right)}
  \label{soln:u0}
  \\
  p_0 &= \frac{\rho g\beta \Delta T a\left(a^2b^2 + 3a^2c^2 + b^2c^2\right)xy}{
    2\left(a^4 b^2 + 3a^4c^2 + a^2b^4 + 2a^2b^2c^2 + 3b^4c^2\right)}
  \label{soln:p0}
\end{align}

%% \begin{figure}\label{fig:p0}
%%   \vspace{16em}
%%   \caption{\textit{TODO:---Insert a figure showing the pressure the
%%       creeping flow in the ellipsoid.}}
%% \end{figure}

%% The isobaric surfaces from equation~(\ref{soln:p0}) are shown for a
%% particular finite ellipsoid in figure~\ref{fig:p0}.
Equation~(\ref{soln:p0-2d}) is the limit of equation~(\ref{soln:p0})
as $c\rightarrow\infty$; similarly, a function $\psi$ can be
produced such that $\nabla\times(\psi\mathbf k)$ gives $\bm u$ from
equation~(\ref{soln:u0}) in each plane of constant $z$:
\begin{equation}
  \psi = \frac{\rho g\beta\Delta T a^3 b^4 c^2\varTheta^2}
       {16\mu(a^4  b^2 + 3 a^4 c^2 + a^2 b^4 + 2  a^2 b^2 c^2 + 3 b^4 c^2)}.
       \label{soln:psi}
\end{equation}

%% Stream-lines, computed from equation~\ref{soln:psi} are drawn in
%% figure~\ref{fig:psi}.
Again, this reduces to equation~(\ref{eq:Bryan}) as
$c\rightarrow\infty$.  Further, it acts as a stream-function in each
plane of constant $z$.  The stream-lines are all ellipses normal to
and centred on the $z$-axis and geometrically similar to the section
through the ellipsoidal boundary of the $xy$-plane.  It is that the
stream-lines are geometrically similar rather than confocal ellipses
that reduces the usefulness for this problem of
Lam\'e's~\cite{lame_37_surfaces} ellipsoidal coordinates.

As $b\rightarrow\infty$, equation~(\ref{soln:u0}) reduces to the
unidirectional vertical solution in the horizontally heated infinite
vertical duct~\cite{mcbain_99_fully}; if, further,
$c\rightarrow\infty$, the classical cubic profile between two parallel
vertical walls~\cite{waldmann_39_theorie} is regained.

%% \begin{figure}\label{fig:psi}
%%   \vspace{16em}
%%   \caption{\textit{TODO:---Insert a figure showing the stream-lines of
%%       the creeping flow in the ellipsoid.  Or perhaps use streamplot?
%%       \protect\url{http://matplotlib.org/examples/images_contours_and_fields/streamplot_demo_features.html}}}
%% \end{figure}

\subsection{First-order correction to the temperature}
The next step of iteration on the temperature equation~(\ref{eq:T})
suggests an expansion in Grashof number $\mathrm{Gr} \equiv 8 \rho^2g \beta \Delta T a^3 / \mu^2$:
\begin{equation}\label{eq:T_expansion}
  T \sim T_0 + \mathrm{Gr}\;\mathrm{Pr}\;T_1 + O \left(\mathrm{Gr}^2\right),
\end{equation}
where $\mathrm{Pr} \equiv \mu C/k$ is the Prandtl number, with the new
term satisfying
\begin{equation}
  \nabla^2 T_1 = \frac{\rho}{\mu\;\mathrm{Gr}}\;\bm u_0 \cdot\nabla T_0,
  \label{eq:T1}
\end{equation}
and vanishing on the boundary.  

The right-hand side is proportional to $y\varTheta$.  This is like
equation~(\ref{eq:u0}), but slightly more complicated.  To facilitate
the method of undetermined coefficients, we build up a library of
functions vanishing on the ellipsoid, i.e. having $\varTheta$ as a
factor, and calculate their laplacians;
cf.~\cite[\S\,8.1.1]{mansfield_64_bending}.  Only polynomials in $x$,
$y$, and $z$ need be considered, since the laplacian operator is
closed in this set, and the right-hand side is of this form.  Further,
symmetry requires that $T_1$ should be even in $x$ and $z$ and odd in
$y$.  Thus:
\begin{equation}
  \nabla^2 (y \varTheta) = \frac{2}{a^2} + \frac{6}{b^2} + \frac{2}{c^2},
\end{equation}
a multiple of which will match a constant on the right-hand side;
\begin{equation}
  \nabla^2 (x^2y\varTheta) = 2y\left\{
  \left(
  \frac{5}{a^2} + \frac{3}{b^2} + \frac{1}{c^2}
  \right) x^2 
  + \varTheta\right\};
\end{equation}
etc., etc.; this suggests the form
\begin{equation}
  T_1 =\left(c_x x^2 + c_y y^2 + c_z z^2 + c_0\right)y\varTheta\Delta T / a
  \label{soln:T1}
\end{equation}
where $c_x, c_y, c_z$ and $c_0$ are constants depending only on the
semiaxes $a, b, c$.  These can be determined from a linear system by
matching the coefficients of $y$, $yx^2$, $yz^2$, and $y^3$:

\begin{align}
  c_x =& b^2 c^2 (a^2b^2 + 7a^2c^2 + b^2c^2) (5a^2b^2 + 3a^2c^2 + b^2c^2)/d_1 d_2
  \\
  c_y =& a^2  c^2  (a^2b^2 + 3a^2c^2 + 5b^2c^2) 
  (5a^2b^2 + 3a^2c^2 + b^2c^2)/d_1 d_2 \\
  c_z =& a^2b^2 (a^2b^2 + 3a^2c^2 + 5b^2c^2) (a^2b^2 + 7a^2c^2 + b^2c^2)/d_1 d_2
  \\
  c_0 =& -a^2  b^2  c^2 (a^2b^2 + 3a^2c^2 + 5b^2c^2) \times\nonumber  \\
       & (a^2b^2 + 7a^2c^2 + b^2c^2)(5a^2b^2 + 3a^2c^2 + b^2c^2)\div \nonumber\\
       & (a^2b^2 + 3a^2c^2 + b^2c^2)d_1 d_2,
\end{align}
where
\begin{align}
  d_1 = &768 (a^6b^6 + 13a^6b^4c^2 + 35a^6b^2c^4 + 15a^6c^6 + 
  \nonumber \\
  & 7a^4b^6c^2 + 62a^4b^4c^4 + 35a^4b^2c^6 + 7a^2b^6c^4 +
  \nonumber \\
  & 13a^2b^4c^6 + b^6c^6) \\
  d_2 =& a^4b^2 + 3a^4c^2 + a^2b^4 + 2a^2b^2c^2 + 3b^4c^2.
\end{align}
$T_1$ is displayed in figure~\ref{fig:T1}.

\begin{figure}
  \hspace{-1em}\includegraphics[width=\columnwidth]{T1.eps}\vspace{-3ex}
  \caption{The first-order correction to the temperature in the plane
    of symmetry $z=0$, from equation~(\ref{soln:T1}); $T_1$ is positive
    in the top half.}\label{fig:T1}
\end{figure}

\subsection{The temperature to first-order}
For low finite Grashof numbers, the temperature field can be
approximated by truncating the asymptotic
expansion~(\ref{eq:T_expansion}) to $T_0 + \mathrm{Gr}\;\mathrm{Pr}
T_1$, as shown in figure~\ref{fig:Tcomposite}.  These are yet to be
compared with numerical solutions of the full nonlinear equations as
done for the sphere~\cite{mcbain_00_low}, but experience there
suggests that these should be reasonably accurate to the level shown.
\begin{figure}
  
  \hspace{-1em}\includegraphics[width=.98\columnwidth]{Tcomposite-1e+03.eps}$10^3$\vspace{-3ex}
  
\hspace{-1em}\includegraphics[width=.98\columnwidth]{Tcomposite-1e+04.eps}$10^4$\vspace{-3ex}

\hspace{-1em}\includegraphics[width=.98\columnwidth]{Tcomposite-1e+05.eps}$10^5$\vspace{-3ex}

%% \hspace{-1em}\includegraphics[width=\columnwidth]{Tcomposite-1e+06.eps}$10^6$\vspace{-3ex}

  \caption{Approximate temperature fields $T_0 +
    \mathrm{Gr}\;\mathrm{Pr} T_1$ in the plane $z=0$ for
    $\mathrm{Gr}\;\mathrm{Pr}$ as labelled.}\label{fig:Tcomposite}
\end{figure}


\section{Dicussion \& conclusions}

The expressions for the pressure, velocity, and temperature given
above for the general ellipsoid simplify considerably when two or
three of the axes have equal length (so that the ellipsoid becomes a
prolate or oblate spheroid or sphere); such simplifications are
straightforward and omitted here.

The creeping flow solution in the ellipse was found by an educated
guess at the form of the pressure field based on those in the sphere
and the ellipse, the former having been found in previous work and the
latter deduced from an analogy with a problem in the theory of elastic
plates.  The zeroth- and first-order temperature fields were found
fairly easily as polynomial solutions of the Poisson equation with
polynomial right-hand sides; this should apply for higher-order
temperature fields too, since the right-hand sides are generated from
lower-order solutions and should remain polynomial, of steadily
increasing degree.

The next step, however, is the first-order correction to the velocity
and pressure; this involves a forced Stokes problem.  The solution is
expected to be polynomial, but no immediate method of solution is at
hand---the poloidal--toroidal decomposition not having been
generalized from the sphere.  The `method' of undetermined
coefficients will work if provided with sufficient candidates; perhaps
a systematic approach could be guided by the enumeration of
polynomial solenoidal vector fields in the ellipsoid used to study
inviscid rotating flows; see Vantieghem~\cite{vantieghem_14_inertial}
and the references therein.

The zeroth-order exact solution is noteworthy in providing a
nontrivial test-case in a simply defined and completely bounded
geometry for steady three-dimensional computational fluid dynamics
codes.  In particular, it is free of the corner-singularities which
complicate lid-driven cavities~\cite{botella_98_benchmark}.

Topologically, the three-dimensional creeping flow of
equation~(\ref{soln:u0}) differs little from the two-dimensional flow
in the ellipse as drawn in figure~\ref{fig:Bryan}; however, if gravity
or the heating of the surrounding solid were not parallel to one of
the axes of the ellipsoid, much more complicated kinematics would
result.  The creeping flow would be the sum of two or three
contributions like equation~(\ref{soln:u0}) for the components of the
conduction temperature field along the various axes; this combination
is unlikely to have closed stream-lines.

%% \textit{TODO:---Consider whether there might an even deeper link
%%   between the ellipsoidal harmonics and the polynomial solutions
%%   considered in this paper, just as there was in the spherical cases.
%%   Maybe reread Green (1835), Niven (1891?), and Dassios (2012).  Maybe
%%   comment on this in the conclusion?}

\subsection{Useful software}

Verifying that the lengthy expressions above satisfied the partial
differential equations and boundary conditions was greatly facilitated
by \textit{SymPy}~\cite{joyner_12_open}.
\textit{Matplotlib}~\cite{hunter_07_matplotlib} and
\textit{Asymptote}~\cite{bowman_08_asymptote} were used to plot the
figures.

\bibliography{mcbain_16_creeping}

\end{document}


* div U = 0

* rho U . grad U = - grad P - rho g beta (T - T0) j + mu lap U

* rho c U . grad T = k lap T

- - -

Then say:

* T -> T0  + theta Delta t

* grad -> grad* / 2 a

  * div -> div* / 2 a
  
  * lap -> lap* / 4 a^2

The temperature equation becomes

* rho c U . grad* theta / 2 a Delta T = k lap* theta / 4 a^2 Delta T

* 2 a (rho c / k) U . grad* theta = lap* theta

Introduce Pr = nu / (k / rho c) = rho c nu / k, so that rho c / k = Pr / nu

* 2 a (Pr / nu) U . grad* theta = lap* theta

Finally, choose u = 2 a U / nu Gr so that

* Gr Pr u . grad theta = lap* theta

which is the desired dimensionless temperature equation.

Then put u = u0 + Gr u1 + O(Gr^2) and similarly for theta to get

* Gr Pr {u0 + Gr u1 + O(Gr^2)} . grad {theta0 + Gr theta1 + O(Gr^2)} =
  lap* {theta0 + Gr theta1 + O(Gr^2)}

* Pr {Gr u0 . grad theta0 + Gr^2(u1 . grad theta0 + u0 . grad theta1) + O(Gr^3)} = lap* theta0 + Gr lap* theta1 + Gr^2 lap* theta2 + O(Gr^3)

* 0 = lap* theta0

* Pr u0 . grad theta0 = lap* theta1

* Pr (u1 . grad theta0 + u0 . grad theta1) = lap* theta2

* ...[eqs 8.15, 8.18]{mcbain_99_vapour}

  Then try u in the continuity equation

div* u0 = 0, div* u1 = 0, &c.

And in the momentum equation

* rho (Gr nu u / 2a) . (grad* / 2a) (Gr nu u / 2a) = - grad* P / 2a -
  rho g beta Delta T theta j + mu nu (lap* / 4a^2) Gr u / 2a

*  Gr u . grad* u = - 4a^2 grad* P / Gr rho nu^2 -
(8 g beta Delta T a^3 / Gr nu^2) theta j + lap* u

*  Gr u . grad* u = - 4a^2 grad* P / Gr rho nu^2 - theta j + lap* u

Put p = 4 a^2 P / Gr rho nu^2 to get

* Gr u . grad* u = - grad* p - theta j + lap* u

  Then expand.

  * Gr (u0 + Gr u1) . grad* (u0 + Gr u1) = - grad* (p0 + Gr p1) - (theta0 + Gr theta1) j + lap* (u0 + Gr u1)

* 0 = -grad* p0 - theta0 j + lap* u0

* u0 . grad* u0 = -grad* p1 - theta1 j + lap* u1

* u0 . grad* u1 + u1 . grad* u0 = - grad* p2 - theta2 j + lap* u2

  * &c., as [eq. 8.20]{mcbain_99_vapour}

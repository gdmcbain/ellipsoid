Say

* d psi = u0 dy - v0 dx

  * curl (k psi) = (d psi / dy) i - (d psi / dx) j

= u0 i + v0 j

so u0 = curl (k psi) = -k x grad psi

Then lap u0 = - curl curl u0 = curl curl (k x grad psi)

= curl (k lap psi - k . grad (grad psi)

= curl (k lap psi)

Or lap u0 = -curl curl curl (k psi)

and

grad p0 = x j - curl curl curl (k psi)

Take curl

0 = 

* Try to revert to using F = (x/a)**2 + (y/b)**2 + (y/c)**2 - 1 as the
  boundary function with F < 0 as the interior. It's just too
  well-engrained in the literature; nondimensionalization would be
  cumbersome.

* Perhaps change the approach to asymptotic expansion to the iterative
  method, as described by Hinch~\cite[p. 2]{hinch_91_perturbation}?

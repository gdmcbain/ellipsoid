#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Temperature to first order in the ellipsoid.

Just draw in the plane of symmetry, z = 0.

For 20AFMC.

:author: G. D. McBain <gdmcbain@protonmail.com>

:created: 2016-08-07

Now, T1 = (cx * x**2 + cy * y**2 + cz * z**2 + c0) y F DeltaT / a; see
../../../sympy/T1.py for constants.

'''

from __future__ import absolute_import, division, print_function

from os.path import splitext
from sys import argv

from matplotlib.patches import Ellipse
import matplotlib.pyplot as plt

import numpy as np
from scipy.constants import golden

def main(name, ar=4/9, Ra=[1e3, 1e4, 1e5]):

    a, b, c = 1, ar, ar**2
    x = np.linspace(-a, a)
    X, Y = np.meshgrid(x, ar * x)

    T0 = X

    dnmntr = (768 * (a**4*b**2 + 3*a**4*c**2 + a**2*b**4 +
                     2*a**2*b**2*c**2 + 3*b**4*c**2) *
              (a**6*b**6 + 13*a**6*b**4*c**2 + 35*a**6*b**2*c**4 +
               15*a**6*c**6 + 7*a**4*b**6*c**2 + 62*a**4*b**4*c**4 +
               35*a**4*b**2*c**6 + 7*a**2*b**6*c**4 + 13*a**2*b**4*c**6 +
               b**6*c**6))
    cx = (b**4 * c**4 * (a**2*b**2 + 7*a**2*c**2 + b**2*c**2) *
          (5*a**2*b**2 + 3*a**2*c**2 + b**2*c**2)) / dnmntr
    cy = (a**2 * b**2 * c**4 *
          (a**2*b**2 + 3*a**2*c**2 + 5*b**2*c**2) *
          (5*a**2*b**2 + 3*a**2*c**2 + b**2*c**2)) / dnmntr
    c0 = (-a**2 * b**4 * c**4 *
          (a**2*b**2 + 3*a**2*c**2 + 5*b**2*c**2) *
          (a**2*b**2 + 7*a**2*c**2 + b**2*c**2) *
          (5*a**2*b**2 + 3*a**2*c**2 + b**2*c**2) /
          (a**2*b**2 + 3*a**2*c**2 + b**2*c**2)) / dnmntr
    F = (X/a)**2 + (Y/b)**2 - 1 # z = 0
    T1 = (cx * X**2 + cy * Y**2 + c0) * Y * F

    for ra in Ra:
        fig, ax = plt.subplots()

        boundary = Ellipse((0, 0), 2 * a, 2 * b, fill=False, linewidth=4)
        ax.add_patch(boundary)

        cs = ax.contour(X, Y, T0 + ra * T1, levels=np.linspace(-a, a, 2**5))

        for collection in cs.collections:
            collection.set_clip_path(boundary)

        ax.set_aspect(1)
        ax.set_xlim(np.array([-a, a]) * 1.02)
        ax.set_ylim(ar * np.array(ax.get_xlim()))
        ax.axis('off')

        fig.savefig('{0}-{1:3.0e}{2}.eps'.format(name[:-4], ra, name[-4:]),
                    bbox_inches='tight')

    
if __name__ == '__main__':
    main(splitext(argv[0])[0], 1/2)

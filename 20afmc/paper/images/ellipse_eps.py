#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Pressure in the ellipse

For 20AFMC.

:author: G. D. McBain <gdmcbain@protonmail.com>

:created: 2016-08-07

Now, p = P0 x y, P0 const, but P0 is irrelevant for the normalized
shading.

'''

from __future__ import absolute_import, division, print_function

from os.path import splitext
from sys import argv

from matplotlib.patches import Ellipse
import matplotlib.pyplot as plt

import numpy as np
from scipy.constants import golden


def main(name, ar=golden - 1):

    a, b = 1, ar
    x = np.linspace(-a, a, 99)
    X, Y = np.meshgrid(x, ar * x)

    fig, ax0 = plt.subplots()
    boundary = Ellipse((0, 0), 2 * a, 2 * b, fill=False, linewidth=4)
    ax0.add_patch(boundary)

    p = X * Y
    cs = ax0.contour(X, Y, p, levels=np.linspace(-a * b / 2, a * b / 2, 2**5))

    for collection in cs.collections:
        collection.set_clip_path(boundary)

    ax0.set_aspect(1)
    ax0.set_xlim(np.array([-1., 1.]) * 1.02)
    ax0.set_ylim(ar * np.array(ax0.get_xlim()))
    plt.axis('off')
    fig.savefig(name + '.eps', bbox_inches='tight')


if __name__ == '__main__':
    main(splitext(argv[0])[0], 1/2)

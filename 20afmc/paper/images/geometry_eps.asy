import three;
import graph3;
currentprojection = oblique;

real a = 4cm;			// x semi-axis
real phi = (1 + sqrt(5)) / 2;	// golden ratio
real ar = 2;
real b = a / ar;		// y semi-axis
real c = b / ar;		// z semi-axis
  
// pair x = (1, 0);		// unit vectors
// pair y = (0, 1);
// pair z = (-(1, 1) / sqrt(8));

real arrowlength = a / 9;
// draw(O--(arrowlength * X), End); // x-axis

xaxis3(xmin=0, xmax=a, arrow=EndArrow3);
yaxis3(ymin=0, ymax=b, arrow=EndArrow3);
zaxis3(zmin=0, zmax=c, arrow=EndArrow3);

label("$x$", (a, 0, 0), E);
label("$y$", (0, b, 0), N);
label("$z$", (0, 0, c), SW);

label("$a$", (a, 0, 0) / 2, N);
label("$b$", (0, b, 0) / 2, W);
label("$c$", (0, 0, c) / 2, NW);

// label("$x$", (arrowlength * X), E);
// draw(O--(arrowlength * Y), EndArrow3); // y-axis
// label("$y$", (arrowlength * Y), N);
// draw(O--(arrowlength * Z), EndArrow3); // z-axis
// label("$z$", (arrowlength * Z), SW);

draw(scale(a, b, 0) * unitcircle3);
draw(scale(a, 0, c) * circle(O, 1, Y), dashed);
draw(scale(0, b, c) * circle(O, 1, X), dashed);


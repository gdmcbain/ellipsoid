#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Isobars in the ellipse

For 20AFMC.

:author: G. D. McBain <gdmcbain@protonmail.com>

:created: 2016-08-06

Now, p = P0 x y, P0 const.

What's the extent of the pressure in the ellipse?  I suppose the
maximum lies where grad p = P0 (y i + x j) is normal to the boundary?
grad F = 2 (x i / a**2 + y j / b**2).  So do we want grad p ^ grad F
to vanish?

I find that it's proportional to (a y)**2 - (b x)**2, and that
vanishes when (b x)**2 = (a y)**2, or b x = (+/-) a y.  This is a pair
of lines through the origin, cutting the boundary at 

   ([+/-] a / sqrt(2), [+/-] b / sqrt(2))

where P0 x y = [+/-] P0 a b / 2, so -P0 a b / 2 < p < P0 a b / 2.

So, each first-quadrant isobar P0 x y = C, with C in P0 a b
np.linspace([-1, 1]) / 2 is of the form y = C / P0 x, with x running
from 0 to wherever it cuts F = 0, which is

   sqrt(a * (a / 2 - sqrt((P0 * a * b / 2)**2 - C**2) / P0 / b))

[to be checked].

But let's rescale so that K = 2 C / P0 a b, or C = P0 a b K / 2, so P0
a b / 2 = C / K and the right abscissa is

   sqrt(a * (a / 2 - a sqrt((C / K)**2 - C**2) / 2 / (P0 a b / 2)))

   sqrt(a * (a / 2 - a sqrt((C / K)**2 - C**2) / 2 / (C / K)))

   sqrt(a * a (1 - sqrt((C / K)**2 - C**2) / (C / K)) / 2)

   sqrt(a * a (1 - sqrt(pmax**2 - p**2) / pmax) / 2)

   sqrt(a**2 (1 - sqrt(1 - (p/pmax)**2)) / 2)

   a * sqrt((1 - sqrt(1 - (p/pmax)**2)) / 2)

Then y = C / P0 x = p / P0 x = p a b / P0 a b x = (p a b / 2) / (P0 a
b / 2) x = (p/pmax) a b / 2 x

Ah, no, actually each cuts the boundary twice!

'''

from __future__ import absolute_import, division, print_function

from os.path import splitext
from sys import argv

from matplotlib.collections import PathCollection, EllipseCollection
from matplotlib.patches import Ellipse
from matplotlib.path import Path
import matplotlib.pyplot as plt

import numpy as np
from scipy.constants import golden


def main(name, ar=golden - 1):

    a, b = 1, ar
    x = np.linspace(-a, a)
    y = ar * x
    X, Y = np.meshgrid(x, y)

    fig, ax0 = plt.subplots()
    boundary = Ellipse((0, 0), 2, 2 * ar, fill=False, linewidth=4)
    ax0.add_patch(boundary)
    # patches = [
    #            for c in np.linspace(0, 1, 12)]
    # ax0.add_collection(PatchCollection(patches, True))

    for p in np.linspace(0, 1, 11)[1:]: # normalized
        x = np.linspace(a * np.sqrt((1 - np.sqrt(1 - p**2)) / 2),
                        a * np.sqrt((1 + np.sqrt(1 - p**2)) / 2))
        y = p * a * b / 2 / x
        ax0.plot(+x, +y, color='k', linestyle='solid') # Q1
        ax0.plot(-x, +y, color='k', linestyle='dashed') # Q2
        ax0.plot(-x, -y, color='k', linestyle='solid') # Q3
        ax0.plot(+x, -y, color='k', linestyle='dashed') # Q4
    
    ax0.set_aspect(1)
    ax0.set_xlim(np.array([-1., 1.]) * 1.02)
    ax0.set_ylim(ar * np.array(ax0.get_xlim()))
    plt.axis('off')
    print(ax0.get_xlim())
    fig.savefig(name + '.eps', bbox_inches='tight')


if __name__ == '__main__':
    main(splitext(argv[0])[0])

#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Velocity in the ellipsoid

For 20AFMC.

:author: G. D. McBain <gdmcbain@protonmail.com>

:created: 2016-08-07

u ~ (a**2 y i - b**2 x j) F

'''

from __future__ import absolute_import, division, print_function

from os.path import splitext
from sys import argv

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d

import numpy as np
from scipy.constants import golden


def main(name, a=1, b=golden-1, c=(golden-1)**2):

    b, c = c, b
    x, y, z = np.meshgrid(np.linspace(-a, a, 13),
                          np.linspace(-b, b, 13),
                          np.linspace(0, c, 4))

    F = (x/a)**2 + (y/b)**2 + (z/c)**2 - 1
    u = +a**2 * y * F
    u[F > 0] = np.ma.masked
    v = -b**2 * x * F
    v[F > 0] = np.ma.masked
    w = 0 * u

    u, v = -u, -v

    # KLUDGE gdmcbain 2016-08-07: Matplotlib lib has z up, x
    # down-right, y up-right.  We want z as our Y and -y as our Z, x
    # is OK.  Ditto for velocities

    y, z = z, -y
    v, w = w, -v

    fig = plt.figure()
    ax0 = fig.gca(projection='3d')
    ax0.quiver(x, y, z, u, v, w, length=0.1)
    # boundary = Ellipse((0, 0), 2 * a, 2 * b, fill=False, linewidth=4)
    # ax0.add_patch(boundary)

    # p = X * Y
    # cs = ax0.contourf(X, Y, p, levels=np.linspace(-a * b / 2, a * b / 2, 32),
    #                   cmap=plt.cm.gray)

    # for collection in cs.collections:
    #     collection.set_clip_path(boundary)

    ax0.set_aspect(1)
    # ax0.set_xlim(np.array([-1., 1.]) * 1.02)
    # ax0.set_ylim(ar * np.array(ax0.get_xlim()))
    # plt.axis('off')
    plt.xlabel('x')
    plt.ylabel('y')
    ax0.set_zlabel('z')
    fig.savefig +(name + '.eps', bbox_inches='tight')
if __name__ == '__main__':
    main(splitext(argv[0])[0])

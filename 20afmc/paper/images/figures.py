#!/usr/bin/env python
# -*- coding: utf-8 -*-

from scipy.constants import golden


def F(x, y, z=np.inf, a=1, b=golden-1, c=np.inf):
    return (x/a)**2 + (y/b)**2 + (z/c)**2 - 1



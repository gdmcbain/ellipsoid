Natural convection in horizontally heated ellipsoidal cavities is
considered in the low Grashof number limit, solving the Laplace
equation for steady thermal conduction in the unbounded solid exterior
and the Boussinesq equations in the fluid-filled interior.

In the hierarchy of equations governing the asymptotic expansion for
small Grashof number, at each order a Stokes problem must be solved
for the momentum correction. The creeping flow is known for the sphere
in closed form in terms of the toroidal and poloidal potentials,
spherical coordinates, and spherical harmonics.  Rather than
attempting to generalize this to ellipsoidal coordinates, it is
re-expressed in terms of the primitive pressure-velocity variables as
polynomials in the Cartesian coordinates.  This form, equivalent in
the sphere, suggests solutions for the pressure in an ellipsoid, which
can then be found together with the velocity in closed form by the
method of undetermined coefficients.  Similarly, the perturbations to
the temperature satisfy Poisson equations which can be solved by the
same method. Explicit formulae are given for the pressure, velocity,
and temperature at the first few orders along with a procedure for
mechanically extending the sequence.

In the limit as one of the axes of the ellipsoid tends to infinity,
the three-dimensional solution reduces to a two-dimensional solution
for natural convection in a horizontal elliptical cylinder,
tranversely horizontally heated.  This exact solution is believed to
be new too.

The zeroth-order exact solution is noteworthy in providing a
nontrivial test-case in a simply defined and completely bounded
geometry for steady three-dimensional computational fluid dynamics
codes.  In particular, it is free of the corner-singularities which
complicate lid-driven cavities for verifying numerical order of
convergence.  This is demonstrated on some free fluid solvers which
are then used to investigate the relation of nonlinear solutions in
ellipsoids to the asymptotic expansion.

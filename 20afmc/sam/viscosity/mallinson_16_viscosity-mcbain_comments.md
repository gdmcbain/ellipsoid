* p. 1: 'The compressibility can be replaced by any reduced property.'
  Compressibility, as defined in the text, is not a 'reduced property'
  in the sense of 'normalization by the critical values'; it's the
  molar volume normalized by the ideal gas value, *P* / *R T*.  Is the
  principle that the compressibility can be replaced by any ratio of
  values of a property, one of which is at a fixed condition?

  + Isn't the *V* in the definition of the compressibility the molar
    volume rather than the 'specific volume'?  I think that it has to
    be for the compressibility to be dimensionless, if *R* is indeed
    the 'universal gas constant'.

  + I see that *V*c has the unit of specific volume in Table 3.

* p. 1: 'The principle is strictly valid only for spherical
  molecules.'  Even then, is it 'strictly' valid?  For
  compressibility, or also when that is replaced by any reduced
  property?  By 'spherical molecules', I assume is meant molecules
  with spherically symmetic laws of attraction, but does the principle
  hold if those laws are different? e.g. rigid sphere
  v. Lennard-Jones?
  
* p. 1. 'a complicated deviation function'.  Maybe say what it is a
  complicated function of.  Is it a functional of the anisotropic part
  of the law of attraction?
  
* p. 1. 'acentric factor'.  Does this need glossing?  (It's not a
  fluid-mechanicsal quantity.)  What is it a function of?  How is the
  difference of *Z* from the spherical reference value factored into
  the acentric factor and the complicated deviation function?

* p. 1, equations 5, 6, 8, 9: Maybe put a comma after the c subscript,
  which denotes critical and is not a variable, and the *ij* subscripts,
  which index the material components

	+ In some house styles, nonvariable subscripts like c would not be
      italicized.

* p. 1: 'for an n component mixture'.  Italicize n.

* p. 1: 'for example OH, CH3, H2O'.  Is H2O a functional group?

* p. 2: Saurez *et al*...Saurez et al'; 'et al' italicized or not?  I
  suggest not, but the main thing is consistency.

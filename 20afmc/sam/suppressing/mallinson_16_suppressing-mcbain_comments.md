* p. 1: '75 A4 ppm': ppm for 'pages per minute' is not a standard
  initialism, indeed it usually stands for 'parts per million'; better
  spell this out
  
* pp. 1, 4:
  'characterised...realized...characterised...visualization': need to
  standardize on -ise or -ize

* figure 2: the arrow-head on the left vortex is too small for its direction to be obvious  

* p. 2: 'backward-difference': this does not imply second order without further specification (there are backward-difference formulæ of orders one through six)

* p. 2: 'smooth spherical particles'.  Is there slip?

* eqs 4, 5: Some division-slashes missing from second RHS of 4 and RHS
  of 5: $\Delta u / \Delta X + \Delta v / \Delta Y$ and $\partial u /
  \partial x + \partial v / \partial y$.
  
* p. 2: 'Maclaurin series...': The index 2 is missing from the big O term.

* p. 2: 'The assumption is...': See if a better symbol is available
  for the comparision operator; e.g. U+226A MUCH LESS-THAN
  
* p. 3: 'nearly Couette flow': Is it?  Whence the bias in figure 6 then?
  
  + Also I wasn't sure in figures 6 and 7 whether these were averages
    over the rows, or if the deflection is in the x or y direction, or
    vectorial.  Since the pressure difference in the y-direction
    matters, I guess it's not x.

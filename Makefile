all:
	${MAKE} -C sympy
	${MAKE} -C 20afmc
clean:
	${MAKE} -C sympy clean
	${MAKE} -C 20afmc clean
.PHONY: all clean

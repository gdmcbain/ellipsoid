# README #

This *README* is formatted in [Markdown](https://bitbucket.org/tutorials/markdowndemo).

This project was commenced 2012-05-25 for the second Gerris meeting in June in Wellington.

It's being revived for the [20th Australasian Fluid Mechanics Conference](http://afms.org.au/20AFMC) in Dec. 2016 at the University of Western Australia.

It had begun using Maxima as computer algebra system, but I might switch to [SymPy](http://sympy.org).
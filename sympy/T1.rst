By iteration,

    k lap T0* = rho c u0 . grad T0

but
    
    grad T0 = grad (x Delta T / 2 a) = i Delta T / 2 a

and
    
u0 . i = rho g beta Delta T a**3 b**2 c**2 y F / 4 mu (a**4 b**2 + 3 a**b c**3 + a**2 b**4 + 2 a**2 b**2 c**2 + 3 b**4 c**2)

Put Gr = 8 rho**2 g beta Delta T a**3 / mu**2

    rho g beta Delta T a**3 / 4 mu = mu Gr / 32 rho

u0 . i = mu Gr b**2 c**2 y F / 32 (a**4 b**2 + 3 a**b c**3 + a**2 b**4 + 2 a**2 b**2 c**2 + 3 b**4 c**2)

lap T0* = (Gr mu c / k) b**2 c**2 y F / 32 (a**4 b**2 + 3 a**b c**3 + a**2 b**4 + 2 a**2 b**2 c**2 + 3 b**4 c**2)

but Pr = nu / alpha = (mu / rho) / (k / rho c) = mu c / k

lap T0* = Gr Pr b**2 c**2 y F / 32 (a**4 b**2 + 3 a**b c**3 + a**2 b**4 + 2 a**2 b**2 c**2 + 3 b**4 c**2)

So put T0* = T0 + Gr T1; i.e. expand in Gr.  We know lap T0 = 0 so

lap T1 = Pr b**2 c**2 y F / 32 (a**4 b**2 + 3 a**b c**3 + a**2 b**4 + 2 a**2 b**2 c**2 + 3 b**4 c**2)

So the basic problem is lap T1 = T10 y F, for T10 const., with T1 = 0
on F = 0.

Now,

grad x**n = n x**(n-1) i

lap x**n = n (n - 1) x**(n-2)

lap F = 2/a**2 + 2/b**2 + 2/c**2

i . grad F = 2 x / a**2

grad x**n . grad F = 2 n x**n / a**2

so using rule for lap(F G) = G lap F + 2 grad G . grad F + F lap G
with G = x**n,

lap (F x**n) =

x**n (2/a**2 + 2/b**2 + 2/c**2) + 4 n x**n / a**2 + n (n - 1) x**(n-2) F

= 2x**n ((2n + 1)/a**2 + 1/b**2 + 1/c**2) + n (n - 1) x**(n - 2) F

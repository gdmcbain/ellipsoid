#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function


from sympy import oo, sqrt
from sympy.vector import curl

from ellipsoid import R, b, c, F as F3D, bilaplacian, mu, buoyancy_term


F = F3D.limit(c, oo)


def on_wall(expr):
    return expr.subs(R.y, b * sqrt((R.y/b)**2 - F))

psi0 = R.k & curl(buoyancy_term, R) / mu / bilaplacian(F**2)
psi = psi0 * F**2

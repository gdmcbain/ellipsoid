* Is the system of spherical coordinate systems defined by McBain
  (1999, PhD, eq. 8.2 -- 8.4) the same as in
  https://en.wikipedia.org/wiki/Spherical_coordinate_system?  The
  latter mentions several conflicting conventions and specifies one as
  approved by the ISO.

McBain had (8.3) tan theta = sqrt(x**2 + y**2) / z, whereas ISO define
cos theta.  Now, cos theta = s / sqrt(1 + tan(theta)**2) = s / sqrt(1
+ (x**2 + y**2) / z = s / sqrt(r**2 / z**2) = s z/r, which agrees with
ISO: cos theta = z/r, so s = +1.  Good.

Here I referred to
https://en.wikipedia.org/wiki/List_of_trigonometric_identities#Related_identities

McBain & ISO agree on r and tan phi = y / x.

Now, sin theta = s sqrt (1 - cos(theta)**2) = s sqrt(1 - z**2 / r**2)
= s sqrt((x**2 + y**2) / r**2) = s sqrt(x**2 + y**2) / r, but sin
theta >= 0 so s = +1 and sin theta = sqrt(x**2 + y**2) / r and 

    r sin theta = sqrt(x**2 + y**2).

Also cos phi = 1 / sqrt(1 + tan(phi)**2) = 1 / sqrt(1 + (y/x)**2) = 1 / sqrt((x**2 + y**2) / x**2) = x / sqrt(x**2 + y**2).  Thus

   r sin theta cos phi = x.

This says T = x (2 + 1 / 8 / r**3) / 3.

That's some kind of spherical harmonic, isn't it.  Surely.  Must be,
anyway.

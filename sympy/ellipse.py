#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Check creeping solution for the ellipse

:author: G. D. McBain <gdmcbain@protonmail.com>

:created: 2016-07-11

'''

from __future__ import absolute_import, division, print_function

from sympy.vector import scalar_potential, curl

from bryan import R, F, mu, buoyancy_term, psi0

u = curl(R.k * psi0 * F**2, R)
p = scalar_potential(buoyancy_term - mu * curl(curl(u, R), R), R)

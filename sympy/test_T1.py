#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function

import unittest

from sympy.vector import gradient

from ellipsoid import R, a, b, c, F, on_wall, laplacian, T0, velocity

from T1 import tau0, T1, csoln, cx, cy, cz, c0


class TestT1(unittest.TestCase):

    def test_bc(self):
        self.assertEqual(on_wall(T1), 0)

    def test_residual(self):
        self.assertEqual(laplacian(T1.subs(csoln)).factor().expand(),
                         (tau0 * velocity.factor() & gradient(T0, R)).expand())

    def test_coefficients(self):
        dnmntr = (768 * (a**4*b**2 + 3*a**4*c**2 + a**2*b**4 +
                         2*a**2*b**2*c**2 + 3*b**4*c**2) *
                  (a**6*b**6 + 13*a**6*b**4*c**2 + 35*a**6*b**2*c**4 +
                   15*a**6*c**6 + 7*a**4*b**6*c**2 + 62*a**4*b**4*c**4 +
                   35*a**4*b**2*c**6 + 7*a**2*b**6*c**4 + 13*a**2*b**4*c**6 +
                   b**6*c**6))
        self.assertEqual((csoln[cz] * dnmntr).factor(),
                         a**2 * b**4 * c**2 *
                         (a**2*b**2 + 3*a**2*c**2 + 5*b**2*c**2) *
                         (a**2*b**2 + 7*a**2*c**2 + b**2*c**2))
        self.assertEqual((csoln[cx] * dnmntr).factor(),
                         b**4 * c**4 *
                         (a**2*b**2 + 7*a**2*c**2 + b**2*c**2) *
                         (5*a**2*b**2 + 3*a**2*c**2 + b**2*c**2))
        self.assertEqual((csoln[cy] * dnmntr).factor(),
                         a**2 * b**2 * c**4 *
                         (a**2*b**2 + 3*a**2*c**2 + 5*b**2*c**2) *
                         (5*a**2*b**2 + 3*a**2*c**2 + b**2*c**2))
        self.assertEqual((csoln[c0] * dnmntr).factor(),
                         -a**2 * b**4 * c**4 *
                         (a**2*b**2 + 3*a**2*c**2 + 5*b**2*c**2) *
                         (a**2*b**2 + 7*a**2*c**2 + b**2*c**2) *
                         (5*a**2*b**2 + 3*a**2*c**2 + b**2*c**2) /
                         (a**2*b**2 + 3*a**2*c**2 + b**2*c**2))

        
if __name__ == '__main__':
    unittest.main()

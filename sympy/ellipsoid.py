#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''A translation of ../maxima/ellipsoid.mac.

This is for checking the exact solution for the creeping velocity in a
horizontally heated ellipsoid.  We assume gravity [0, -g, 0] and
excess boundary temperature proportional to x, with the ellipsoid's
axes Cartesian.

For 20afmc.

:author: G. D. McBain <gmcbain>

:created: 2016-06-22

'''

from __future__ import absolute_import, division, print_function

from sympy import symbols, sqrt, solve
from sympy.vector import CoordSysCartesian, gradient, divergence, curl
from sympy.abc import a, b, c

R = CoordSysCartesian('R')


def laplacian(u, sys=R):
    return divergence(gradient(u, sys), sys)


def bilaplacian(*args, **kwargs):
    return laplacian(laplacian(*args, **kwargs))

F = (R.x/a)**2 + (R.y/b)**2 + (R.z/c)**2 - 1
rho, g, beta, DeltaT, mu = symbols('rho g beta DeltaT mu')


def on_wall(expr):
    return expr.subs(R.y, b * sqrt((R.y/b)**2 - F))


T0 = DeltaT * R.x / 2 / a
buoyancy_term = rho * g * beta * T0 * R.j

P0, u0, v0 = symbols('P0 u0 v0')
p = P0 * R.x * R.y

u0 = (gradient(p, R) & R.i) / mu / laplacian(R.y * F)
u = u0 * R.y * F
v0 = ((gradient(p, R) - buoyancy_term) & R.j) / mu / laplacian(R.x * F)
v = v0 * R.x * F
P01 = solve(divergence(u * R.i + v * R.j, R), P0)[0]
pressure = P01 * R.x * R.y
velocity = (u0 * R.y * R.i + v0 * R.x * R.j).subs(P0, P01) * F

# TRICKY gdmcbain 2016-08-02: The stream-function.  u = d psi / d y, v
# = -d psi / d x.  Can we rearrange this to solve using
# sympy.vector.scalar_potential, as done for the pressure in the
# ellipse?  The gradient of the stream-function is d psi / d x = -v, d
# psi / d y u, so grad psi = -v * R.i + u * R.j == R.k ^ (u * R.i + v
# * R.j) = R.k ^ velocity.

# But I tried 'scalar_potential(R.k ^ velocity, R)' and found:
# 'ValueError: Field is not conservative'.  Is it?  No, curl(R.k ^
# velocity, R) is not zero.

# This is because the scalar potential needs to be found in each plane
# of constant z separately, but even then, scalar_potential(R.k ^ (u *
# R.i + v * R.j).subs(R.z, symbols('z')), R) fails because curl(R.k ^
# (u * R.i + v * R.j).subs(R.z, symbols('z')), R) is not zero.

# Integrating a**2 y F w.r.t. y gives (2 F - (y/b)**2) a**2 y**2 / 4,
# and -(-b**2 x F) w.r.t. x gives (2 F - (x/a)**2) b**2 x**2 / 4), so
# suggesting psi = ...

# ...but why not just try psi proportional to F**2?  As in 2-D?

psi0 = ((velocity & R.i) / (curl(F**2 * R.k, R) & R.i)).factor()
psi = psi0 * F**2



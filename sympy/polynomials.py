#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''For 20afmc

:author: G. D. McBain <gdmcbain@protonmail.com>

:created: 2016-07-23

'''

from __future__ import absolute_import, division, print_function

from sympy import Symbol
from sympy.vector import gradient

from ellipsoid import R, F, laplacian

n = Symbol('n', integer=True, nonnegative=True)


def main():
    print('lap F:', laplacian(F))
    print('lap xF:', laplacian(R.x * F))
    print('lap yF:', laplacian(R.y * F))
    print('lap zF:', laplacian(R.z * F))
    print('lap x**n:', laplacian(R.x**n).simplify())
    print('grad x**n:', gradient(R.x**n, R).simplify())
    print('i . grad F:', R.i & gradient(F, R))
    print('grad x**n . grad F:', gradient(R.x**n, R) & gradient(F, R))
    print('lap x**n F:', laplacian(R.x**n * F))
    print('lap y x**2n F:', laplacian(R.y * R.x**(2*n) * F))


if __name__ == '__main__':
    main()

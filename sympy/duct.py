#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Check Boussinesq's solution in the ellipse.

According to McBain (1999, PhD, eq. 7.66), the vertical velocity field
V = -Py (1 - 4 * (x**2 + z**)) / 8 satisfies the Poisson equation
equation, Py = mu lap V in (x/a)**2 + (y/b)**2 < 1 and V = 0 on the
wall.

:author: G. D. McBain <gdmcbain@protonmail.com>

:created: 2016-07-11

'''


from __future__ import absolute_import, division, print_function

import sympy
from sympy.vector import CoordSysCartesian, gradient, divergence
from sympy.abc import a, b, c

R = CoordSysCartesian('R')

def laplacian(u, sys=R):
    return divergence(gradient(u, sys), sys)
    
def main():

    Py, mu = sympy.symbols('Py mu')
    V = - (Py * (a*c)**2 * (1 - (R.x/a)**2 - (R.z/c)**2) /
           2 / mu / (a**2 + c**2))

    print('mu lap V - Py:', (mu * laplacian(V) - Py).ratsimp())

    def on_wall(expr):
        return expr.subs(R.z, c * sympy.sqrt(1 - (R.x/a)**2))
    print('On wall:', on_wall(V))

if __name__ == '__main__':
    main()

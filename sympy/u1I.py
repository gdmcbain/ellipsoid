#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''How far can we get with the first-order flow correction for inertia

(McBain 2001, JFM, Section 4.4)

:author: G. D. McBain <gdmcbain@protonmail.com>

:created: 2016-08-03

'''

from __future__ import absolute_import, division, print_function

# The forcing term is like u0 . grad u0.

# Here u0 = k a^2 y F, v0 = - k b^2 x F, so

# noting grad F = 2 (x/a^2, y/b^2, z/c^2)

# u0 . (d u0 / dx) + v0 . (d u0 / dy) = (k a^2 y)^2 F dF/dx -k b^2 x F
# * k a^2 F = 2 (k a^2 y)^2 F x / a^2 - (k a b F)^2 x = 2 (k a y)^2 F
# x - (k a b F)^2 x = 



# u0 . (d v0 / dx) + v0 . (d v0 / dy) = 

# I found that 'velocity & gradient(velocity & R.i, R)' was
# proportional to x F**2, 'velocity & gradient(velocity & R.j, R)' to
# y F**2, and of course velocity & R.k = 0.

# And 

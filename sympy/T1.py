#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''

:author: G. D. McBain <gdmcbain@protonmail.com>

:created: 2016-07-28

'''

from __future__ import absolute_import, division, print_function

from sympy import solve, symbols
from sympy.vector import gradient

from ellipsoid import (R, a, F, laplacian, rho, g, beta, DeltaT, mu,
                       T0, velocity)

# In full: rho cp u . grad T = k lap T, but say u = u0 and T = T0 + Gr
# Pr T1, then, since lap T0 = 0

# rho cp u0 . grad T0 ~ k Gr Pr lap T1

# Pr = nu / kappa = (mu / rho) / (k / rho cp) = mu cp / k

# rho u0 . grad T0 ~ mu Gr lap T1

# lap T1 = (rho / mu Gr) u0 . grad T0 == tau0 u0 . grad T0

# rho / mu Gr = mu / 8 rho g beta DeltaT a**3 == tau0

# k, cp = symbols('k cp')         # thermal conductivity, specific heat
# Pr = mu * cp / k                # Prandtl number
Pr = symbols('Pr')

cx, cy, cz, c0 = symbols('cx cy cz c0')
tau0 = mu / (8 * rho * g * beta * DeltaT * a**3)
T1 = (cx * R.x**2 + cy * R.y**2 + cz * R.z**2 + c0) * R.y * F * DeltaT / a

T1res = (laplacian(T1) -
         ((tau0 * velocity).factor() & gradient(T0, R))).expand()
T1res_y1 = T1res.coeff(R.y)
variables = [cx, cy, cz, c0]
csoln = solve([T1res_y1.coeff(R.x, 0).coeff(R.z, 0),
               T1res_y1.coeff(R.x, 2),
               T1res_y1.coeff(R.z, 2),
               T1res.coeff(R.y, 3)],
              variables)
# cx, cy, cz, c0 = [csoln[v] for v in variables]

# Note that there is a solution, with a different normalization, in
# ../maxima/ellipsoid.mac; there, it looks as though cx, cy, and cz
# have a common denominator.

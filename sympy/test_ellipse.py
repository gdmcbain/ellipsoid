#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Check creeping solution for the ellipse

:author: G. D. McBain <gdmcbain@protonmail.com>

:created: 2016-07-28

'''

from __future__ import absolute_import, division, print_function

import unittest

from sympy.vector import gradient, divergence, curl, Vector

from ellipsoid import a, b, rho, g, beta, DeltaT
from bryan import on_wall
from ellipse import R, mu, buoyancy_term, u, p


class TestPressure(unittest.TestCase):

    def test_divergence(self):
        self.assertEqual(divergence(u, R), 0)

    def test_noslip(self):
        self.assertEqual(on_wall(u).factor(), Vector.zero)

    def test_pressure(self):
        self.assertEqual((p / R.x / R.y).factor(),
                         rho * g * beta * DeltaT * a *
                         (3 * a**2 + b**2) / 2 /
                         (3 * a**4 + 2*a**2*b**2 + 3*b**4))

    def test_momentum(self):
        self.assertEqual(gradient(p, R).factor(),
                         (buoyancy_term -
                         mu * curl(curl(u, R), R)).factor())


if __name__ == '__main__':
    unittest.main()

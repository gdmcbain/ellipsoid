#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''For 20afmc

:author: G. D. McBain <gdmcbain@protonmail.com>

:created: 2016-07-28

'''

import unittest

from ellipsoid import R, a, b, c, F, laplacian
from polynomials import n


class TestPolynomials(unittest.TestCase):

    def test_xn(self):
        self.assertEqual(
            laplacian(R.x**n * F).expand(),
            (2 * R.x**n * ((2 * n + 1) / a**2 + 1/b**2 + 1/c**2) +
             n * (n - 1) * R.x**(n - 2) * F).expand())

    def test_yn(self):
        self.assertEqual(
            laplacian(R.y**n * F).expand(),
            (2 * R.y**n * (1/a**2 + (2 * n + 1)/b**2 + 1/c**2) +
             n * (n - 1) * R.y**(n - 2) * F).expand())

    def test_zn(self):
        self.assertEqual(
            laplacian(R.z**n * F).expand(),
            (2 * R.z**n * (1/a**2 + 1/b**2 + (2 * n + 1)/c**2) +
             n * (n - 1) * R.z**(n - 2) * F).expand())

    def test_yF(self):
        self.assertEqual(
            laplacian(R.y * F).expand(),
            2 * (R.y * (1/a**2 + 3/b**2 + 1/c**2)).expand())

    def test_x2yF(self):
        self.assertEqual(laplacian(R.x**2 * R.y * F).expand(),
                         2 * (R.y * ((R.x**2 * (5/a**2 + 3/b**2 + 1/c**2)) +
                                     F)).expand())


if __name__ == '__main__':
    unittest.main()

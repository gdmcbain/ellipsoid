#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Check my solution for natural convection in the elliptic duct.

McBain (1999, JFM; 1999, PhD)

:author: G. D. McBain <gdmcbain@protonmail.com>

:created: 2016-07-11

'''


from __future__ import absolute_import, division, print_function

import sympy
from sympy.vector import CoordSysCartesian, gradient, divergence
from sympy.abc import a, b, c

R = CoordSysCartesian('R')

def laplacian(u, sys=R):
    return divergence(gradient(u, sys), sys)
    
def main():

    k = 1 / (3/a**2 + 1/c**2) / 2
    V = k * R.x * (1 - (R.x/a)**2 - (R.z/c)**2)

    print('R.x + lap V:', (R.x + laplacian(V)).ratsimp())

    def on_wall(expr):
        return expr.subs(R.z, c * sympy.sqrt(1 - (R.x/a)**2))
    print('On wall:', on_wall(V))

if __name__ == '__main__':
    main()

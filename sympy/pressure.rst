The thing about a pressure field proportional to xy is that grad xy =
yi + xj, so div grad p = 0; it cannot match any divergence in the
buoyancy term.

However, if the buoyancy term is of the form xj, it is solenoidal, so
no problem.

But, if we have such a field, can we compute the velocity?

    lap u[0] = dp / dx = p0 * R.y

    lap u[1] = x + dp / dy = (1 + p0) * R.x

    lap u[2] = dp / dz = 0 => u[2] = 0

The first two of these are like equation (3.17) of
\ref{mcbain_99_fully}.  That was two-dimensional in the ellipse and
its solution suggests u[0] = u00 * R.y * F, u[1] = u10 * R.x * F.

Then

    div u = 2 * R.x * R.y * (a**2 * u10 + b**2 * u00) / (a * b)**2

so u10 = -u00 * (b/a)**2.  Or say

    u[0] = -u0 * R.y * F / b**2,

    u[1] = u0 * R.x * F / a**2

But this says u = curl(-u0 k F**2) / 4; i.e. u has stream-function -u0
F**2 / 4, different in each plane of constant z.

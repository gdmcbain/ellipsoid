#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function

import unittest

from sympy.vector import gradient, curl, Vector

from ellipsoid import R, a, b, rho, g, beta, DeltaT, mu
from bryan import psi, psi0, buoyancy_term, bilaplacian, on_wall


class TestBryan(unittest.TestCase):

    def test_boundary_conditions(self):
        self.assertEqual(on_wall(psi).factor(), 0)
        self.assertEqual(on_wall(gradient(psi, R)).factor(), Vector.zero)

    def test_bilap(self):
        u = curl(R.k * psi, R)
        self.assertEqual(bilaplacian(psi).factor(),
                         (R.k & curl(curl(curl(u, R), R), R)).factor())

    def test_force_curl(self):
        self.assertEqual(R.k & curl(buoyancy_term, R),
                         rho * g * beta * DeltaT / 2 / a)

    # # Therefore, -mu lap lap psi = rho g beta DeltaT / 2 a

    # # -16 rho a**4 lap lap psi / mu**2 =

    # # = 8 rho**2 g beta DeltaT a**3 / mu**2 == Gr

    def test_psi0(self):
        self.assertEqual(psi0.factor(),
                         (rho * g * beta * DeltaT * a**3 * b**4 / 16 /
                          mu / (3 * a**4 + 2 * a**2 * b**2 + 3 * b**4)))


if __name__ == '__main__':
    unittest.main()

#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
:author: G. D. McBain <gmcbain>

:created: 2016-07-28

'''

from __future__ import absolute_import, division, print_function

import unittest

from sympy.vector import Vector, gradient, divergence, curl

from ellipsoid import (R, a, b, c, F, on_wall, rho, g, beta, DeltaT, mu,
                       buoyancy_term, velocity, pressure, psi)


class TestVelocityApproach(unittest.TestCase):

    def test_noslip(self):
        self.assertEqual(on_wall(velocity), Vector.zero)

    def test_continuity(self):
        self.assertEqual(divergence(velocity, R).factor(), 0)

    def test_momentum(self):
        self.assertEqual(gradient(pressure, R),
                         (buoyancy_term -
                          mu * curl(curl(velocity, R), R)).factor())

    def test_coefficients(self):
        self.assertEqual((pressure / R.x / R.y).factor(),
                         rho * g * beta * DeltaT * a *
                         (a**2 * b**2 + 3 * a**2*c**2 + b**2*c**2) / 2 /
                         (a**4 * b**2 + 3 * a**4 * c**2 + a**2 * b**4 +
                          2 * a**2 * b**2 * c**2 + 3*b**4 * c**2))
        self.assertEqual(((velocity & R.i) / R.y / F).factor(),
                         rho * g * beta * DeltaT * a**3 * b**2 * c**2 / 4 /
                         mu / (a**4*b**2 + 3*a**4*c**2 + a**2*b**4 +
                               2*a**2*b**2*c**2 + 3*b**4*c**2))
        self.assertEqual(((velocity & R.j) / R.x / F).factor(),
                         -rho * g * beta * DeltaT * a * b**4 * c**2 / 4 /
                         mu / (a**4*b**2 + 3*a**4*c**2 + a**2*b**4 +
                               2*a**2*b**2*c**2 + 3*b**4*c**2))

    def test_streamfunction(self):
        self.assertEqual(curl(psi * R.k, R).factor(), velocity.factor())
        self.assertEqual(on_wall(psi), 0)
        self.assertEqual(on_wall(gradient(psi, R)), Vector.zero)

    def test_streamfunction_coefficient(self):
        self.assertEqual(psi / F**2,
                         rho * g * beta * DeltaT * a**3 * b**4 * c**2 /
                         16 / mu / (a**4 * b**2 + 3 * a**4 * c**2 +
                                    a**2 * b**4 + 2 * a**2 * b**2 * c**2 +
                                    3 * b**4 * c**2))
                                    
    
        

if __name__ == '__main__':
    unittest.main()

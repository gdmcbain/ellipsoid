#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Check the temperature field outside the sphere.

According to McBain (1999, PhD, eq. 8.5), the temperature field T =
(1/8/r**3 + 2) / 3 * r * sin(theta) * cos(phi) satisfies the Laplace
equation, dT/dr = 0 on r = 1/2, and T ~ 2x/3 (r -> oo).

Try this as preliminary to the exterior of the ellipsoid.

:author: G. D. McBain <gdmcbain@protonmail.com>

:created: 2016-07-07

'''


from __future__ import absolute_import, division, print_function

import sympy
from sympy.vector import CoordSysCartesian, gradient, divergence

R = CoordSysCartesian('R')
r, theta, phi = sympy.symbols('r theta phi', positive=True)


def to_spherical(expr):
    return expr.subs({R.x: r * sympy.sin(theta) * sympy.cos(phi),
                      R.y: r * sympy.sin(theta) * sympy.sin(phi),
                      R.z: r * sympy.cos(theta)})


def main():

    T = R.x / 3 * (2 + sympy.Rational(1, 8) / r**3).subs(
        r, sympy.sqrt(R.x**2 + R.y**2 + R.z**2))

    q = gradient(T, R)
    s = divergence(q, R)
    print('lap T:', s.ratsimp())

    print('far-field x-gradient as [x, y, z] -> oo:',
          [(q & R.i).limit(x, sympy.oo) for x in [R.x, R.y, R.z]])
    rvec = R.x * R.i + R.y * R.j + R.z * R.k

    def on_wall(expr):
        return to_spherical(expr).trigsimp().subs(r, sympy.Rational(1, 2))

    print('Wall gradient:', on_wall(q & rvec))
    print('Wall value:', on_wall(T) / on_wall(R.x) * R.x)

if __name__ == '__main__':
    main()

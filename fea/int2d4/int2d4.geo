lc = 1e-2;
Point(1) = {0, 0, 0};
Point(2) = {1, 0, 0, lc};
Point(3) = {0, 0.5, 0, lc};
Ellipse(1) = {2, 1, 2, 3};
Line(2) = {3, 1};
Line(3) = {1, 2};
Line Loop(4) = {1, 2, 3};
Plane Surface(5) = {4};
Physical Surface(6) = {5};
Physical Line(7) = {1};		//wall
Physical Line(8) = {2};		//symmetry, y-axis, x=0
Physical Line(9) = {3};		//symmetry, x-axis y=0

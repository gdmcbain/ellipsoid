from SCons.Script import Clean, Glob

Clean('~', Glob('*~'))
Clean('vtk', Glob('*.vtu') + Glob('*.pvd'))

#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Solve for temperature in interior of quarter of ellipse.

given Dirichlet values on wall varying linearly with x.

For 20AFMC.

:author: G. D. McBain <gdmcbain@protonmail.com>

:created: 2016-11-15

Then to do Stokes...

  0 = - grad p + x j + lap u

  0 = - <v, grad p> + <v, x j> + <v, lap u>

  0 = <div v, p> - [v, p n] + <v, x j> - <grad v, grad u> + [v, n grad u]

  <grad v, grad u> - <div v, p> = <v, x j> + [v, n grad u - n p].

'''

from __future__ import absolute_import, division, print_function

from os.path import splitext

from firedrake import (Mesh, Constant, Expression, DirichletBC, File,
                       VectorFunctionSpace, FunctionSpace, Function,
                       TrialFunction, TestFunction, inner, grad, dx,
                       ds, assemble, solve)


def area(mesh):
    return assemble(Constant(1., mesh) * dx)


def lengths(mesh, faces):
    return [assemble(Constant(1., mesh) * ds(face)) for face in faces]


def heat(mesh):
    V = FunctionSpace(mesh, 'Lagrange', 2)
    u = Function(V, name='temperature')

    solve(inner(grad(TestFunction(V)), grad(u)) * dx == 0, u,
          bcs=[DirichletBC(V, 0., 8),
               DirichletBC(V, Expression('x[0]'), 7)])
    return u


def stokes(mesh):
    V = VectorFunctionSpace(mesh, 'Lagrange', 2)
    Q = FunctionSpace(mesh, 'Lagrange', 1)
    W = V * Q

    u, p = TrialFunctions(W)
    v, q = TestFunctions(W)
    f = Expression(('0.0', 'x[0]'))
    a = inner(grad(u), grad(v)) + div(v) * p + q * div(u)
    L = inner(f, v) * dx

    noslip = Constant((0., ) * 2)

    


def main(name, meshfile):
    mesh = Mesh(meshfile)
    print('Area:', area(mesh))
    print('Lengths:', lengths(mesh, [7, 8, 9]))

    File(name + '_temperature.pvd').write(heat(mesh))
    


if __name__ == '__main__':

    from sys import argv

    main(splitext(argv[0])[0], argv[1])

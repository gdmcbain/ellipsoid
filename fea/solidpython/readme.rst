STL
---

There's a problem with the SolidPython + OpenSCAD approach: the STL
rendered by the latter is fine as defining the geometry but very poor
quality in the terms of the finite element method.

The surface would need to be remeshed.

https://geuz.org/trac/gmsh/wiki/STLRemeshing

http://www.cfd-online.com/Forums/openfoam-meshing-open/155141-gmsh-remesh-stl.html

...and many others.

Alternatives:

- freecad

  + use OpenSCAD workbench, export as BRep

- distmesh ?

FreeCAD
-------

FreeCAD has an OpenSCAD workbench, so it can
`importCSG.insert('object.scad', 'name')`; however, this seems to be
incomplete on the version 0.15 (revision 4671 git) installed on
besant.  It raises a ValueError: "invalid literal for int() with base
10: '03-1'".  I believe that this has been fixed in version 0.16, as
installed on laplace.

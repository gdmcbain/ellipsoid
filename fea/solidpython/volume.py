#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Read mesh from Gmsh into Firedrake.

For 20AFMC.

:author: G. D. McBain <gdmcbain@protonmail.com>

:created: 2016-11-03

'''

from __future__ import absolute_import, division, print_function

import firedrake


def volume(mesh):
    return firedrake.assemble(firedrake.Constant(1., mesh) * firedrake.dx)


def main(name, meshfile):
    print('Volume: ', volume(firedrake.Mesh(meshfile)))


if __name__ == '__main__':

    from os.path import splitext
    from sys import argv

    main(splitext(argv[0])[0], argv[1])

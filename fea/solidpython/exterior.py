#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Exterior to an ellipsoid.

For 20AFMC.

:author: G. D. McBain <gdmcbain@protonmail.com>

:created: 2016-11-10

'''

from __future__ import absolute_import, division, print_function

import solid

import ellipsoid


def main(name, a=None, b=None, symmetry=None):

    solid.scad_render_to_file(ellipsoid.exterior(a, b) *
                              ellipsoid.symmetry([3*bi for bi in b],
                                                 symmetry))


if __name__ == '__main__':

    from os.path import splitext
    from sys import argv

    a = [1/a for a in [2, 3, 5]]
    b = [5 * a for a in a]
    
    main(splitext(argv[0])[0], a, b, *argv[1:])

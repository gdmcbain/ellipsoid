#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''First attempt at building an ellipsoid.

For 20AFMC.

:author: G. D. McBain <gdmcbain@protonmail.com>

:created: 2016-11-03

'''

from __future__ import absolute_import, division, print_function

import numpy as np

import solid
import solid.utils


def ellipsoid(a=None, sym=None):
    '''return an ellipsoid

    :param a: sequence of three floats > 0, semiaxes

    :param sym: optional str, 'hemi' (z < 0) or 'octant' (x > 0,
    y > 0, z < 0).  (The negative halfspace was chosen for z as then
    the face z == 0 resembles a traditional two-dimensional ellipse.)

    :rtype: solid.solidpython.scale or solid.solidpython.intersection
    if symmetry is not None

    '''

    a = [1.] * 3 if a is None else a
    print('Theoretical volume:', volume(a, symmetry))
    o = solid.scale(a)(solid.sphere())

    if sym is not None:

        # KLUDGE gmcbain 2016-11-04: There's no halfspace in OpenSCAD
        # (or SolidPython) so hemiellipsoids, &c. need to be
        # intersections with cubes. See
        # http://forum.openscad.org/most-primitive-primitive-feature-request-td5159.html

        safety = 3.
        assert(safety > 2.)

        o *= symmetry([safety * ai for ai in a], sym)

    return o


def symmetry(b, sym=None):
    'factor to enforce symmetry'
    
    return solid.translate({'hemi': [0, 0, -b[2] / 2],
                            'octant': [b[0] / 2, b[1] / 2, -b[2] / 2]}
                           [sym])(solid.cube(b, center=True))


def exterior(a, b):
    '''return the part of a cube outside an ellipsoid

    :param a: sequence of three floats > 0, semiaxes

    :param b: sequence of three floats > 0, side-lengths

    '''

    return solid.cube(b) - ellipsoid(a)
    


def volume(a, symmetry=None):
    '''return the volume of the ellipsoid with semiaxes a

    :param a: sequence of three floats > 0, semiaxes

    :param symmetry: optional str, 'hemi' (z < 0) or 'octant' (x > 0,
    y > 0, z < 0).  (The negative halfspace was chosen for z as then
    the face z == 0 resembles a traditional two-dimensional ellipse.)

    :rtype: float >= 0

    Ref.: Dassios (2012, p. 113, eq. 6.25).

    '''

    return 4 / 3 * np.pi * np.prod(a) / {'hemi': 2,
                                         'octant': 8}.get(symmetry, 1)


def main(name, a=None, symmetry=None):

    solid.scad_render_to_file(ellipsoid(a, symmetry))


if __name__ == '__main__':

    from os.path import splitext
    from sys import argv

    main(splitext(argv[0])[0],
         [1/a for a in [2, 3, 5]],
         *argv[1:])

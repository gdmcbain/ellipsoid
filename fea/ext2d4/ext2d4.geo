lc = 1e-2;
Point(1) = {0, 0, 0};
Point(2) = {1, 0, 0, lc};
Point(3) = {0, 0.5, 0, lc};
Ellipse(1) = {2, 1, 2, 3};

Point(4) = {9, 0, 0};
Point(5) = {9, 9, 0};
Point(6) = {0, 9, 0};

Line(2) = {2, 4};
Line(3) = {4, 5};
Line(4) = {5, 6};
Line(5) = {6, 3};
Line Loop(6) = {4, 5, -1, 2, 3};
Plane Surface(7) = {6};
Physical Surface(8) = {7};
Physical Line(9) = {1};		//wall
Physical Line(10) = {5};	//symmetry, y-axis, x=0
Physical Line(11) = {2};	//symmetry, x-axis, y=0
Physical Line(12) = {3, 4};	//distal

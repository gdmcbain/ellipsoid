#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Solve for temperature in exterior of quarter of ellipse.

given Dirichlet values on symmetry-line x = 0 and in far-field.

For 20AFMC.

:author: G. D. McBain <gdmcbain@protonmail.com>

:created: 2016-11-19

'''

from __future__ import absolute_import, division, print_function

from os.path import splitext

from firedrake import (Mesh, Constant, Expression, DirichletBC, File,
                       VectorFunctionSpace, FunctionSpace, Function,
                       TrialFunction, TestFunction, inner, grad, dx,
                       ds, assemble, solve)


def main(name, meshfile):
    mesh = Mesh(meshfile)

    V = FunctionSpace(mesh, 'Lagrange', 2)
    u = Function(V, name='temperature')

    solve(inner(grad(TestFunction(V)), grad(u)) * dx == 0, u,
          bcs=[DirichletBC(V, Expression('x[0]'), (10, 12))])

    File(name + '.pvd').write(u)
    


if __name__ == '__main__':

    from sys import argv

    main(splitext(argv[0])[0], argv[1])

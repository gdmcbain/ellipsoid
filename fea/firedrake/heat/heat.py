#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Solve for temperature in interior of ellipsoid,

given Dirichlet values on wall varying linearly with x.

For 20AFMC.

:author: G. D. McBain <gdmcbain@protonmail.com>

:created: 2016-11-04

'''

from __future__ import absolute_import, division, print_function

from os.path import splitext

from firedrake import (Mesh, Constant, Expression, DirichletBC, File,
                       FunctionSpace, Function, TestFunction,
                       inner, grad, dx, assemble, solve)


def volume(mesh):
    return assemble(Constant(1., mesh) * dx)


def main(name, meshfile):
    mesh = Mesh(meshfile)
    V = FunctionSpace(mesh, 'CG', 1)
    u = Function(V, name='temperature')

    solve(inner(grad(TestFunction(V)), grad(u)) * dx == 0, u,
          bcs=[DirichletBC(V, 0., 9),
               DirichletBC(V, Expression('x[0]'), 12)],
          solver_parameters={'ksp_type': 'minres'})

    File(name + '.pvd').write(u)

    print('Volume: ', volume(Mesh(meshfile)))


if __name__ == '__main__':

    from sys import argv

    main(splitext(argv[0])[0], argv[1])

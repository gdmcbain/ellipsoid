Point(1) = {-0.5, 0, 0};
Point(2) = {0.5, 0, 0};
Point(3) = {0.5, 0.618, 0};
Point(4) = {-0.5, 0.618, 0};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};

Line Loop(5) = {1, 2, 3, 4};
Plane Surface(6) = {5};

Physical Surface("domain", 1) = {6};
Physical Line("wall", 2) = {1, 2, 3, 4};


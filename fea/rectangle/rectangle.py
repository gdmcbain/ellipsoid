#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Borrowed from

https://fenicsproject.org/qa/5287/using-the-petsc-pcfieldsplit-in-fenics

and adapted for Firedrake

:author: G. D. McBain <gdmcbain@protonmail.com>

:created: 2016-11-23

'''

from __future__ import absolute_import, division, print_function

from os.path import splitext
from functools import partial

from matplotlib.patches import Rectangle
import matplotlib.pyplot as plt
import numpy as np

from firedrake import (Mesh, VectorFunctionSpace, FunctionSpace,
                       Function, TestFunctions, TrialFunctions,
                       TestFunction, Constant, SpatialCoordinate,
                       interpolate, DirichletBC, LinearSolver, solve,
                       assemble, dx, inner, grad, div, File, plot)


def main(filename):

    name = splitext(filename)[0]

    mesh = Mesh(filename)

    # temperature

    V = FunctionSpace(mesh, 'CG', 2)
    x = SpatialCoordinate(V.mesh())[0]

    T = Function(V)
    v = TestFunction(V)
    solve(inner(grad(v), grad(T)) * dx == 0, T,
          bcs=DirichletBC(V, interpolate(x, V), 2),
          solver_parameters={'ksp_type': 'cg',
                             'pc_type': 'hypre',
                             'pc_hypre_type': 'boomeramg',
                             'pc_hypre_boomeramg_strong_threshold': 0.75,
                             'pc_hypre_boomeramg_agg_nl': 2,
                             'pc_hypre_boomeramg_max_levels': 25})

    File(name + '_T.pvd').write(T)

    # flow

    W = VectorFunctionSpace(mesh, 'CG', 2) * FunctionSpace(mesh, 'CG', 1)

    u, p = TrialFunctions(W)
    v, q = TestFunctions(W)

    A, P = map(partial(assemble,
                       bcs=DirichletBC(W.sub(0),
                                       Constant((0.,) *
                                                mesh.topological_dimension()),
                                       2),
                       mat_type='nest'),
               [(inner(grad(u), grad(v)) + div(v) * p + q * div(u)) * dx,
                (inner(grad(u), grad(v)) + p * q) * dx])
    b = assemble(inner(T, v[1]) * dx)

    parameters = {
        'ksp_type': 'minres',
        'pc_type': 'fieldsplit',
        'pc_fieldsplit_type': 'schur',
        'pc_fieldsplit_schur_fact_type': 'full',
        'pc_fieldsplit_schur_precondition': 'selfp',

        'fieldsplit_0_ksp_type': 'preonly',
        'fieldsplit_0_pc_type': 'bjacobi',
        'fieldsplit_1_ksp_type': 'preonly',
        'fieldsplit_1_pc_type': 'hypre',
        'fieldsplit_1_pc_hypre_type': 'boomeramg',
        'fieldsplit_1_pc_hypre_boomeramg_strong_threshold': 0.75,
        'fieldsplit_1_pc_hypre_boomeramg_agg_nl': 2
    }

    solver = LinearSolver(A, P, solver_parameters=parameters)
    U = Function(W)
    solver.solve(U, b)

    velocity, pressure = U.split()
    pressure *= -1
    File(name + '_u.pvd').write(velocity)
    File(name + '_p.pvd').write(pressure)

    # stream-function

    psi = Function(V)
    v = TestFunction(V)

    solve(inner(grad(v), grad(psi)) * dx -
          inner(v, velocity[0].dx(1) - velocity[1].dx(0)) * dx == 0,
          psi,
          bcs=DirichletBC(V, 0, 2),
          solver_parameters={'ksp_type': 'cg',
                             'pc_type': 'hypre',
                             'pc_hypre_type': 'boomeramg',
                             'pc_hypre_boomeramg_strong_threshold': 0.75,
                             'pc_hypre_boomeramg_agg_nl': 2,
                             'pc_hypre_boomeramg_max_levels': 25})

    File(name + '_stream.pvd').write(psi)

    # fig, ax = plt.subplots()
    # plot(T, axes=ax, contour=True)
    # plot(psi, axes=ax, contour=True,
    #      colors='k', linestyles='solid')
    # ax.axis('off')
    # fig.savefig(name + '_stream')


    fig, ax = plt.subplots()

    # from firedrake.plot import _two_dimension_triangle_func_val
    # triangulation, temperature = _two_dimension_triangle_func_val(T, 1)
    # ax.tricontourf(triangulation, temperature, 2**9,
    #                cmap=plt.get_cmap('bwr'))

    X, Y = np.meshgrid(np.linspace(-.5, .5),
                       np.linspace(0, .618))

    ax.contour(X, Y, X, 2**7, cmap=plt.get_cmap('bwr'))
    ax.contourf(X, Y, X, 2**7, cmap=plt.get_cmap('bwr'))
    boundary = Rectangle((-.5, 0), 1, .618, fill=False, linewidth=4)
    ax.add_patch(boundary)
    plot(psi, axes=ax, contour=True,
         colors='k', linestyles='solid')
    plt.axis('off')
    plt.savefig(name)
    


if __name__ == '__main__':

    from sys import argv

    main(argv[1])

from SCons.Script import Glob, Clean

Clean('~', Glob('*~'))
Clean('vtk', Glob('*.pvtu') + Glob('*.vtu') + Glob('*.pvd'))
Clean('msm', Glob('msm*'))
Clean('images', Glob('*.png'))


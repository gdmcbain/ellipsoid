#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Read mesh from Gmsh into Firedrake.

For 20AFMC.

:author: G. D. McBain <gdmcbain@protonmail.com>

:created: 2016-11-03

'''

from __future__ import absolute_import, division, print_function

from firedrake import Mesh, Constant, dx, ds, assemble


def volume(mesh):
    return assemble(Constant(1., mesh) * dx)

def areas(mesh, faces):

    # For comparison, the quarter-ellipses in the planes of symmetry
    # are:

    # np.pi / np.array([a * b for a, b in product([2, 3, 5], repeat=2) if a < b])

    # ...but the area of the curved  surface is trickier: 'Just as the
    # length  of the  perimeter of  an ellipse  cannot be  obtained in
    # closed form  because it  is given by  an elliptic  integral, the
    # surface area of an ellipsoid is  also given in terms of elliptic
    # integrals.' (Dassios 2012, p. 265)

    return [assemble(Constant(1., mesh) * ds(face)) for face in faces]


    
def main(name, meshfile):
    print('Volume: ', volume(Mesh(meshfile)))
    print('Areas: ', areas(Mesh(meshfile), range(9, 13)))


if __name__ == '__main__':

    from os.path import splitext
    from sys import argv

    main(splitext(argv[0])[0], argv[1])

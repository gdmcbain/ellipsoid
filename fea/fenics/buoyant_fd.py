#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Borrowed from

https://fenicsproject.org/qa/5287/using-the-petsc-pcfieldsplit-in-fenics

and adapted to two dimensions.

:author: G. D. McBain <gdmcbain@protonmail.com>

:created: 2016-11-19

'''

from __future__ import absolute_import, division, print_function

import numpy as np

import petsc4py.PETSc as PETSc
from firedrake import (Mesh, VectorFunctionSpace, FunctionSpace,
                       Function, TestFunctions, TrialFunctions,
                       Constant, Expression, interpolate, DirichletBC,
                       LinearSolver, assemble, dx, inner, grad, div,
                       File)


def main(filename):

    mesh = Mesh(filename)
    V = VectorFunctionSpace(mesh, 'CG', 2)
    Q = FunctionSpace(mesh, 'CG', 1)
    W = V * Q

    u, p = TrialFunctions(W)
    v, q = TestFunctions(W)
    f = interpolate(Expression(('0.0', 'x[0]')), V)
    a = (inner(grad(u), grad(v)) + div(v) * p + q * div(u)) * dx
    L = inner(f, v) * dx
    m = (inner(grad(u), grad(v)) + p * q) * dx

    bcs = [DirichletBC(W.sub(0), Constant((0., ) * 2), 7)]
    A = assemble(a, bcs=bcs, mat_type='nest')
    b = assemble(L)
    M = assemble(m, bcs=bcs, mat_type='nest')

    parameters = {
        'ksp_type': 'minres',
        'ksp_tol': 1e-8,
        'pc_type': 'fieldsplit',
        'pc_fieldsplit_type': 'schur',
        'pc_fieldsplit_schur_fact_type': 'full',
        
        "fieldsplit_0_ksp_type": "cg",
        "fieldsplit_0_pc_type": "ilu",
        "fieldsplit_0_ksp_rtol": 1e-12,
        "fieldsplit_1_ksp_type": "cg",
        "fieldsplit_1_pc_type": "none",
        "fieldsplit_1_ksp_rtol": 1e-12,
    }

    solver = LinearSolver(A, M, solver_parameters=parameters)
    U = Function(W)
    solver.solve(U, b)

    velocity, pressure = U.split()
    
    File('buoyant_velocity.pvd').write(velocity)
    File('buoyant_pressure.pvd').write(pressure)

if __name__ == '__main__':

    from sys import argv
    
    main(*argv[1:])

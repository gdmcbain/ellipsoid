#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Borrowed from

https://fenicsproject.org/qa/5287/using-the-petsc-pcfieldsplit-in-fenics

and adapted to two dimensions.

:author: G. D. McBain <gdmcbain@protonmail.com>

:created: 2016-11-19

'''

from __future__ import absolute_import, division, print_function

import numpy as np

import petsc4py.PETSc as PETSc
from dolfin import (UnitSquareMesh, VectorFunctionSpace,
                    FunctionSpace, MixedElement, Function,
                    TestFunctions, TrialFunctions, Constant,
                    Expression, DirichletBC, DOLFIN_EPS, KrylovSolver,
                    LUSolver, assemble_system, dx, inner, grad, div,
                    as_backend_type, File)


def main(n=8):
    mesh = UnitSquareMesh(8, 8)
    V = VectorFunctionSpace(mesh, 'CG', 2)
    Q = FunctionSpace(mesh, 'CG', 1)
    W = V * Q

    def right(x, on_boundary): return x[0] > (1.0 - DOLFIN_EPS)

    def left(x, on_boundary): return x[0] < DOLFIN_EPS

    def top_bottom(x, on_boundary):
        return x[1] > 1.0 - DOLFIN_EPS or x[1] < DOLFIN_EPS

    noslip = Constant((0.0, 0.0))
    bc0 = DirichletBC(W.sub(0), noslip, top_bottom)
    inflow = Expression(('-sin(x[1]*pi)', '0.0'))
    bc1 = DirichletBC(W.sub(0), inflow, right)
    zero = Constant(0)
    bc2 = DirichletBC(W.sub(1), zero, left)
    bcs = [bc0, bc1, bc2]
    (u, p) = TrialFunctions(W)
    (v, q) = TestFunctions(W)
    f = Constant((0.0, 0.0))
    a = inner(grad(u), grad(v)) * dx + div(v) * p * dx + q * div(u) * dx
    L = inner(f, v) * dx
    m = inner(grad(u), grad(v))*dx + p * q * dx
    (A, b) = assemble_system(a, L, bcs)
    (M, _) = assemble_system(m, L, bcs)

    # fenics direct solve
    solver = KrylovSolver('tfqmr', 'amg')
    solver.set_operators(A, M)
    U = Function(W)
    solver.solve(U.vector(), b)

    velocity, pressure = U.split()
    File('velocity.pvd') << velocity
    File('pressure.pvd') << pressure    
    
    A = as_backend_type(A).mat()
    b = as_backend_type(b).vec()
    M = as_backend_type(M).mat()

    # fieldsplit solve
    ksp = PETSc.KSP().create()
    ksp.setType(PETSc.KSP.Type.TFQMR)
    pc = ksp.getPC()
    pc.setType(PETSc.PC.Type.FIELDSPLIT)
    is0 = PETSc.IS().createGeneral(W.sub(0).dofmap().dofs())
    is1 = PETSc.IS().createGeneral(W.sub(1).dofmap().dofs())
    pc.setFieldSplitIS(('u', is0), ('p', is1))
    pc.setFieldSplitType(0)  # 0=additive
    subksps = pc.getFieldSplitSubKSP()
    subksps[0].setType("preonly")
    subksps[0].getPC().setType("hypre")
    subksps[1].setType("preonly")
    subksps[1].getPC().setType("hypre")
    ksp.setOperators(A, M)
    ksp.setFromOptions()
    (x, _) = A.getVecs()
    ksp.solve(b, x)

    print(max(abs(U.vector().array())),
          max(abs(U.vector().array() - x.array)))


if __name__ == '__main__':
    main()

#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Borrowed from

https://fenicsproject.org/qa/5287/using-the-petsc-pcfieldsplit-in-fenics

and adapted to two dimensions.

:author: G. D. McBain <gdmcbain@protonmail.com>

:created: 2016-11-19

'''

from __future__ import absolute_import, division, print_function

import numpy as np

import petsc4py.PETSc as PETSc
from dolfin import (Mesh, VectorFunctionSpace,
                    FunctionSpace, Function, TestFunctions,
                    TrialFunctions, Constant, Expression, DirichletBC,
                    DOLFIN_EPS, KrylovSolver, LUSolver,
                    assemble_system, dx, inner, grad, div,
                    as_backend_type, File)


def main(filename):

    mesh = Mesh(filename)
    V = VectorFunctionSpace(mesh, 'CG', 2)
    Q = FunctionSpace(mesh, 'CG', 1)
    W = V * Q

    noslip = Constant((0.0, 0.0))
    bc0 = DirichletBC(W.sub(0), noslip, 7)
    bc1 = DirichletBC(W.sub(0), noslip, 8)
    bc2 = DirichletBC(W.sub(0), noslip, 9)
    bcs = [bc0, bc1, bc2]
    u, p = TrialFunctions(W)
    v, q = TestFunctions(W)
    f = Expression(('0.0', 'x[0] - 0.5'))
    a = (inner(grad(u), grad(v)) + div(v) * p + q * div(u)) * dx
    L = inner(f, v) * dx
    m = (inner(grad(u), grad(v)) + p * q) * dx
    A, b = assemble_system(a, L, bcs)
    M, _ = assemble_system(m, L, bcs)

    solver = KrylovSolver('tfqmr', 'amg')
    solver.set_operators(A, M)
    U = Function(W)
    solver.solve(U.vector(), b)

    velocity, pressure = U.split()
    File('buoyant_velocity.pvd') << velocity
    File('buoyant_pressure.pvd') << pressure    

if __name__ == '__main__':

    from sys import argv
    
    main(*argv[1:])

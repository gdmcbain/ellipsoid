from SCons.Script import Glob, Clean

Clean('~', Glob('*~'))
Clean('vtk', Glob('*.vtu') + Glob('*.pvd'))


#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Solve for temperature in exterior of quarter of ellipse.

given Dirichlet values on symmetry-line x = 0 and in far-field.

For 20AFMC.

:author: G. D. McBain <gdmcbain@protonmail.com>

:created: 2016-11-19

'''

from __future__ import absolute_import, division, print_function

from os.path import splitext

from matplotlib.colors import Normalize
from matplotlib.patches import Ellipse
import matplotlib.pyplot as plt

from firedrake import (Mesh, Constant, Expression, DirichletBC, File,
                       VectorFunctionSpace, FunctionSpace, Function,
                       TrialFunction, TestFunction, inner, grad, dx,
                       ds, assemble, solve)


def main(name, meshfile):
    mesh = Mesh(meshfile)

    V = FunctionSpace(mesh, 'Lagrange', 2)
    u = Function(V, name='temperature')

    solve(inner(grad(TestFunction(V)), grad(u)) * dx == 0, u,
          bcs=[DirichletBC(V, Expression('x[0]'), 1)])

    File(name + '.pvd').write(u)

    from firedrake.plot import _two_dimension_triangle_func_val

    fig, ax = plt.subplots()
    triangulation, T = _two_dimension_triangle_func_val(u, 1)

    ax.tricontour(triangulation, T, 2**7, colors='k',
                  norm=Normalize(-2, 2))
    ax.tricontourf(triangulation, T, 2**7, cmap=plt.get_cmap('bwr'),
                   norm=Normalize(-2, 2))
    boundary = Ellipse((0, 0), 2 * 1, 2 * .5, fill=False, linewidth=4)
    ax.add_patch(boundary)
    ax.axis('off')
    ax.set_xlim((-2, 2))
    ax.set_ylim((-1, 1))
    ax.set_aspect('equal')
    fig.savefig(name)
    


if __name__ == '__main__':

    from sys import argv

    main(splitext(argv[0])[0], argv[1])

lc = 1e-2;
Point(1) = {0, 0, 0};
Point(2) = {1, 0, 0, lc};
Point(3) = {0, 0.5, 0, lc};
Point(4) = {-1, 0, 0, lc};
Point(5) = {0, -0.5, 0, lc};
Ellipse(1) = {2, 1, 2, 3};

Point(6) = {9, 9, 0};
Point(7) = {-9, 9, 0};
Point(8) = {-9, -9, 0};
Point(9) = {9, -9, 0};
//+
Ellipse(2) = {3, 1, 4, 4};
//+
Ellipse(3) = {4, 1, 4, 5};
//+
Ellipse(4) = {5, 1, 2, 2};
//+
Line(5) = {9, 6};
//+
Line(6) = {6, 7};
//+
Line(7) = {7, 8};
//+
Line(8) = {8, 9};

Line Loop(9) = {8, 5, 6, 7};
Line Loop(10) = {4, 1, 2, 3};
Plane Surface(11) = {9, 10};
Physical Line(1) = {6, 7, 5, 8}; //distal
Physical Line(2) = {4, 1, 2, 3}; //cavity
Physical Surface(1) = {11};

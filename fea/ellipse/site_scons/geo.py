from SCons.Script import AddOption, GetOption, Environment, Builder

AddOption('--clscale', type=float, nargs=1, help='mesh size factor')

env = Environment(
    CLSCALE=('-clscale %e ' % GetOption('clscale')
             if GetOption('clscale') else ''),
    BUILDERS={
        'Msh': Builder(action='gmsh -2 $CLSCALE $SOURCE',
                       suffix='msh', src_suffix='geo')})

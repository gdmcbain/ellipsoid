Point(1) = {0, 0, 0};
Point(2) = {1, 0, 0};
Point(3) = {0, 0.618, 0};
Point(4) = {-1, 0, 0};
Point(5) = {0, -0.618, 0};

Ellipse(1) = {2, 1, 2, 3};
Ellipse(2) = {3, 1, 4, 4};
Ellipse(3) = {4, 1, 4, 5};
Ellipse(4) = {5, 1, 2, 2};

Line Loop(5) = {2, 3, 4, 1};
Plane Surface(6) = {5};

Physical Surface("domain", 1) = {6};
Physical Line("wall", 2) = {1, 2, 3, 4};

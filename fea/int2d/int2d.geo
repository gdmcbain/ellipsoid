lc = 1e-1;
Point(1) = {0, 0, 0};
Point(2) = {1, 0, 0, lc};
Point(3) = {0, 0.5, 0, lc};
Ellipse(1) = {2, 1, 2, 3};

Point(4) = {-1, 0, 0, lc};
Point(5) = {0, -0.5, 0, lc};

Ellipse(2) = {3, 1, 4, 4};
Ellipse(3) = {4, 1, 4, 5};
Ellipse(4) = {5, 1, 2, 2};
Line Loop(5) = {2, 3, 4, 1};
Plane Surface(6) = {5};
Physical Surface(7) = {6};
Physical Line(8) = {2, 3, 4, 1};
